package org.gs4tr.dataforce.datacrunch.tool.service.file.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

import org.apache.commons.io.FilenameUtils;
import org.gs4tr.dataforce.datacrunch.tool.service.file.FilenameService;

public class FilenameServiceImpl implements FilenameService
{
    @Override
    public Map<String, String> getInfoMapFromAnvilFileName(String anvilFileName)
    {
        Map<String, String> infoMap = new HashMap<>();
        // remove parent folder, and keep only filename
        anvilFileName = FilenameUtils.getName(anvilFileName);
        Matcher matcher = anvilFileNamePattern.matcher(anvilFileName);
        if (!matcher.find() || matcher.groupCount() != 5)
        {
            return infoMap;
        }
        infoMap.put(SUBJECT_ID, matcher.group(1));
        infoMap.put(COMPANY, matcher.group(2));
        infoMap.put(SESSION_ID, matcher.group(3));
        infoMap.put(PROMPT_ID, matcher.group(5));
        return infoMap;
    }

    @Override
    public Map<String, String> getInfoMapFromSatisfactionFileName(String satisfactionFileName)
    {
        Map<String, String> infoMap = new HashMap<>();
        // remove parent folder, and keep only filename
        satisfactionFileName = FilenameUtils.getName(satisfactionFileName);
        Matcher matcher = satisfactionFileNamePattern.matcher(satisfactionFileName);
        if (!matcher.find() || matcher.groupCount() != 2)
        {
            return infoMap;
        }
        infoMap.put(SUBJECT_ID, matcher.group(1));
        infoMap.put(SESSION_ID, matcher.group(2));
        return infoMap;
    }

    @Override
    public Map<String, String> getInfoMapFromPromptFileName(String promptFileName)
    {
        Map<String, String> infoMap = new HashMap<>();
        // remove parent folder, and keep only filename
        promptFileName = FilenameUtils.getName(promptFileName);
        Matcher matcher = promptFileNamePattern.matcher(promptFileName);
        if (!matcher.find() || matcher.groupCount() != 1)
        {
            return infoMap;
        }
        infoMap.put(SUBJECT_ID, matcher.group(1));
        return infoMap;
    }

    @Override
    public Map<String, String> getInfoMapFromHypothesisFileName(String hypothesisFileName)
    {
        Map<String, String> infoMap = new HashMap<>();
        // remove parent folder, and keep only filename
        hypothesisFileName = FilenameUtils.getName(hypothesisFileName);
        Matcher matcher = hypothesisFileNamePattern.matcher(hypothesisFileName);
        if (!matcher.find() || matcher.groupCount() != 1)
        {
            return infoMap;
        }
        infoMap.put(COMPANY, matcher.group(1));
        return infoMap;
    }

}
