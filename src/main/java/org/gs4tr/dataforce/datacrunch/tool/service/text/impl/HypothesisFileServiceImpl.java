package org.gs4tr.dataforce.datacrunch.tool.service.text.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.gs4tr.dataforce.datacrunch.tool.Messages;
import org.gs4tr.dataforce.datacrunch.tool.model.Hypothesis;
import org.gs4tr.dataforce.datacrunch.tool.repository.HypothesisRepository;
import org.gs4tr.dataforce.datacrunch.tool.service.file.FileService;
import org.gs4tr.dataforce.datacrunch.tool.service.file.FilenameService;
import org.gs4tr.dataforce.datacrunch.tool.service.html.HtmlReaderService;
import org.gs4tr.dataforce.datacrunch.tool.service.string.StringHelper;
import org.gs4tr.dataforce.datacrunch.tool.service.text.HypothesisFileService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.TextFileReaderService;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;

public class HypothesisFileServiceImpl implements HypothesisFileService
{
    @Autowired
    private TextFileReaderService textFileReaderService;

    @Autowired
    private FilenameService filenameService;

    @Autowired
    private HtmlReaderService htmlReaderService;

    @Autowired
    private HypothesisRepository hypothesisRepository;

    @Override
    public void processHypothesisFiles(List<String> hypothesisFiles, Map<String, Set<String>> hypothesisMapping)
    {
        if (hypothesisFiles == null || hypothesisFiles.isEmpty())
        {
            LOG.warn(Messages.getString("HypothesisService.1"));
            return;
        }
        for (String hypothesisFile : hypothesisFiles)
        {
            String fileName = FilenameUtils.getName(hypothesisFile);
            LOG.info(String.format(Messages.getString("HypothesisService.2"), fileName));
            if (fileName.endsWith(FileService.HTML_EXTENSION))
            {
                processGoogleHtmlFile(hypothesisFile, hypothesisMapping);
            }
            else
            {
                processHypothesisFile(hypothesisFile, hypothesisMapping);
            }
        }
    }

    @Override
    public void processHypothesisFile(String hypothesisFile, Map<String, Set<String>> hypothesisMapping)
    {
        // get company name from filename:
        String company = "";
        Map<String, String> infoMap = filenameService.getInfoMapFromHypothesisFileName(hypothesisFile);
        if (infoMap.containsKey(FilenameService.COMPANY))
        {
            company = infoMap.get(FilenameService.COMPANY);
        }
        else
        {
            LOG.warn(String.format(Messages.getString("HypothesisService.5"), hypothesisFile));
        }

        List<String> contents = textFileReaderService.getContents(hypothesisFile);
        String[] header = getHeader(contents.get(0));

        for (int i = 1, max = contents.size(); i < max; i++)
        {
            String content = contents.get(i);
            processHypothesisContent(hypothesisFile, content, company, header, hypothesisMapping);
        }
    }

    @Override
    public void processGoogleHtmlFile(String hypothesisFile, Map<String, Set<String>> hypothesisMapping)
    {
        String fileName = FilenameUtils.getName(hypothesisFile);
        Document doc = htmlReaderService.getDocument(hypothesisFile);
        if (doc == null)
        {
            LOG.error(String.format(Messages.getString("HypothesisService.5"), fileName));
            return;
        }
        Elements elements = htmlReaderService.getContentDivElements(doc);
        for (Element element : elements)
        {
            String text = element.wholeText();
            if (!StringUtils.startsWithIgnoreCase(text, CONTENT_PREFIX))
            {
                continue;
            }
            String wholeText = element.wholeText();
            Matcher matcher = CONTENT_PATTERN.matcher(wholeText);
            if (!matcher.find() || matcher.groupCount() != 4)
            {
                LOG.warn(String.format(Messages.getString("HypothesisGoogleHtmlService.3"), wholeText, fileName));
                continue;
            }
            Set<String> set = hypothesisMapping.getOrDefault(GOOGLE, new HashSet<>());
            String hypothesisText = StringHelper.removeQuote(matcher.group(1).trim());
            if (StringUtils.isEmpty(hypothesisText) || set.contains(hypothesisText))
            {
                continue;
            }
            set.add(hypothesisText);
            hypothesisMapping.put(GOOGLE, set);
            Hypothesis hypothesis = new Hypothesis();
            hypothesis.setCompany(GOOGLE.toLowerCase());
            hypothesis.setHypothesis(hypothesisText);

            String dateText = matcher.group(2).trim();
            setDate(dateText, hypothesis, hypothesisFile);
        }
    }

    private String[] getHeader(String content)
    {
        return content.split(CSV_SPLITTED_WITH_QUOTE);
    }

    private void processHypothesisContent(String hypothesisFile, String content, String company,
        String[] header, Map<String, Set<String>> hypothesisMapping)
    {
        if (StringUtils.isBlank(content))
        {
            return;
        }
        // split the content by comma:
        String[] splitted = content.split(CSV_SPLITTED_WITH_QUOTE);
        Hypothesis hypothesis = null;

        // process amazon content:
        if (StringUtils.equalsIgnoreCase(AMAZON, company))
        {
            if (splitted.length != 6)
            {
                LOG.warn(String.format(Messages.getString("HypothesisService.3"), hypothesisFile, content));
                return;
            }
            Set<String> set = hypothesisMapping.getOrDefault(AMAZON, new HashSet<>());
            String summary = splitted[0];
            if (StringUtils.isEmpty(summary) || set.contains(summary))
            {
                return;
            }
            set.add(summary);
            hypothesisMapping.put(AMAZON, set);
            hypothesis = new Hypothesis();
            hypothesis.setCompany(company.toLowerCase());
            processHypothesisContent(splitted, hypothesis, company);
        }
        else if (StringUtils.equalsIgnoreCase(APPLE, company))
        {
            if (splitted.length != 6)
            {
                LOG.warn(String.format(Messages.getString("HypothesisService.3"), hypothesisFile, content));
                return;
            }
            Set<String> set = hypothesisMapping.getOrDefault(APPLE, new HashSet<>());
            String recognitionText = StringHelper.removeQuote(splitted[0]);
            if (StringUtils.isBlank(recognitionText) || StringUtils.equals("*", recognitionText) ||
                set.contains(recognitionText))
            {
                return;
            }
            set.add(recognitionText);
            hypothesisMapping.put(APPLE, set);
            hypothesis = new Hypothesis();
            hypothesis.setCompany(company);
            processHypothesisContent(splitted, hypothesis, company);
        }
        else if (StringUtils.equalsIgnoreCase(GOOGLE, company))
        {
            if (splitted.length != 3 && splitted.length != 6)
            {
                LOG.warn(String.format(Messages.getString("HypothesisService.3"), hypothesisFile, content));
                return;
            }
            Set<String> set = hypothesisMapping.getOrDefault(GOOGLE, new HashSet<>());
            String hypothesisText = "";
            // special file Google001.csv with 3 columns
            // and first column: Date, second column: Data, third column: number
            if (splitted.length == 3)
            {
                hypothesisText = splitted[1];
                if (StringUtils.isEmpty(hypothesisText) || set.contains(hypothesisText))
                {
                    return;
                }
                set.add(hypothesisText);
                hypothesisMapping.put(GOOGLE, set);
                hypothesis = new Hypothesis();
                hypothesis.setCompany(company);
                processGoogleSpecialHypothesisContent(splitted, hypothesis, hypothesisFile);
            }
            else
            {
                hypothesisText = splitted[0];
                if (StringUtils.isEmpty(hypothesisText) || set.contains(hypothesisText))
                {
                    return;
                }
                set.add(hypothesisText);
                hypothesisMapping.put(GOOGLE, set);
                hypothesis = new Hypothesis();
                hypothesis.setCompany(company);
                processHypothesisContent(splitted, hypothesis, company);
            }
        }

        if (hypothesis != null)
        {
            hypothesisRepository.save(hypothesis);
        }
    }

    private void processHypothesisContent(String[] splitted, Hypothesis hypothesis, String company)
    {
        String hypothesisText = StringHelper.removeQuote(splitted[0]);
        String date_month = splitted[1];
        String date_mday = splitted[2];
        String date_hour = splitted[3];
        String date_minute = splitted[4];
        String date_second = splitted[5];

        hypothesis.setHypothesis(hypothesisText);
        // Google uses integer to represent Month, other two use text to represent month
        if (StringUtils.equalsIgnoreCase(company, GOOGLE))
        {
            int monthInt = Integer.parseInt(date_month);
            date_month = getMonth(monthInt);
        }
        hypothesis.setMonth(date_month);
        hypothesis.setDay(Integer.parseInt(date_mday));
        hypothesis.setHour(Integer.parseInt(date_hour));
        hypothesis.setMinute(Integer.parseInt(date_minute));
        hypothesis.setSecond(Integer.parseInt(date_second));
    }

    private void processGoogleSpecialHypothesisContent(String[] splitted, Hypothesis hypothesis, String hypothesisFile)
    {
        String dateText = StringHelper.removeQuote(splitted[0]);
        dateText = StringHelper.removeQuote(dateText);
        // Try to remove any invalid characters.
        dateText = StringHelper.fixInvalidXmlChars(dateText);
        setDate(dateText, hypothesis, hypothesisFile);
        String hypothesisText = splitted[1];
        hypothesis.setHypothesis(hypothesisText);
    }

    private void setDate(String dateText, Hypothesis hypothesis, String hypothesisFile)
    {
        if (StringUtils.isNotEmpty(dateText))
        {
            Date date = null;
            try
            {
                date = new SimpleDateFormat(DATE_FORMAT_PARSE_REGEX).parse(dateText);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                hypothesis.setMonth(getMonth(cal.get(Calendar.MONTH)));
                hypothesis.setDay(cal.get(Calendar.DATE));
                hypothesis.setHour(cal.get(Calendar.HOUR));
                hypothesis.setMinute(cal.get(Calendar.MINUTE));
                hypothesis.setSecond(cal.get(Calendar.SECOND));
            }
            catch (ParseException ex)
            {
                LOG.warn(String.format(Messages.getString("HypothesisService.4"), hypothesisFile, ex.getMessage()));
            }
        }
    }

    private String getMonth(int idx)
    {
        switch (idx)
        {
        case 1:
            return "January";
        case 2:
            return "February";
        case 3:
            return "March";
        case 4:
            return "April";
        case 5:
            return "May";
        case 6:
            return "June";
        case 7:
            return "July";
        case 8:
            return "Auguest";
        case 9:
            return "September";
        case 10:
            return "October";
        case 11:
            return "November";
        case 12:
            return "December";
        default:
            return "";
        }
    }
}
