package org.gs4tr.dataforce.datacrunch.tool.repository;

import java.util.Optional;

import org.gs4tr.dataforce.datacrunch.tool.model.Prompt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("promptRepository")
public interface PromptRepository extends JpaRepository<Prompt, Long>
{
    public Optional<Prompt> findBySubjectIdAndPromptId(int subjectId, int promptId);
}
