package org.gs4tr.dataforce.datacrunch.tool.service.excel;

import java.io.IOException;
import java.util.List;

import org.gs4tr.dataforce.datacrunch.tool.model.ExcelInfo;
import org.springframework.stereotype.Service;

@Service("ExcelWriterService")
public interface ExcelWriterService
{
    static public final String NA = "n/a";

    public void write(String outputFileName, List<ExcelInfo> excelInfos, String type) throws IOException;
}
