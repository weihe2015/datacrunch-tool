package org.gs4tr.dataforce.datacrunch.tool.repository;

import java.util.Optional;

import org.gs4tr.dataforce.datacrunch.tool.model.Satisfaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("satisfactionRepository")
public interface SatisfactionRepository extends JpaRepository<Satisfaction, Long>
{
    public Optional<Satisfaction> findBySessionIdAndSubjectIdAndPromptId(int sessionId, int subjectId, int promptId);
}
