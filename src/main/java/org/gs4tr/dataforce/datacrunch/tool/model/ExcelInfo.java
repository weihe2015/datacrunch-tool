package org.gs4tr.dataforce.datacrunch.tool.model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ExcelInfo
{
    private String fileName;
    private int subjectId;
    private String country;
    private String company;
    private String device;
    private int sessionId;
    private String prompt;
    private String domain;
    private String position;
    private String bargeIn;
    private String relevancyGrading;
    private Date recodingDate;
    private double buttonPress;
    private double userStart;
    private double userEnd;
    private String reference;
    private String hypothesis;
    private int hypothesisWER = -1;
    private double textDisplayStart;
    private double textDisplayEnd;
    private double actionStart;
    private String systemResponse;
    private String category;
    private String subCategory;
    private int superBadResult;
    private String responseVisual;
    private String responseAudio;
    private String format;
    private String result;
    private double ttsStart;
    private double ttsEnd;
    private String coder;

    // only available in speakers
    private int invocationAttempts;
    private double lightOn;
    private double musicDuckingOn;
    private double musicDuckingOff;
    private double lightOff;

    public ExcelInfo()
    {

    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public int getSubjectId()
    {
        return subjectId;
    }

    public void setSubjectId(int subjectId)
    {
        this.subjectId = subjectId;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getCompany()
    {
        return company;
    }

    public void setCompany(String company)
    {
        this.company = company;
    }

    public String getDevice()
    {
        return device;
    }

    public void setDevice(String device)
    {
        this.device = device;
    }

    public int getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(int sessionId)
    {
        this.sessionId = sessionId;
    }

    public String getPrompt()
    {
        return prompt;
    }

    public void setPrompt(String prompt)
    {
        this.prompt = prompt;
    }

    public String getDomain()
    {
        return domain;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getPosition()
    {
        return position;
    }

    public void setPosition(String position)
    {
        this.position = position;
    }

    public String getBargeIn()
    {
        return bargeIn;
    }

    public void setBargeIn(String bargeIn)
    {
        this.bargeIn = bargeIn;
    }

    public String getRelevancyGrading()
    {
        return relevancyGrading;
    }

    public void setRelevancyGrading(String relevancyGrading)
    {
        this.relevancyGrading = relevancyGrading;
    }

    public Date getRecodingDate()
    {
        return recodingDate;
    }

    public String getRecordDateInFormat()
    {
        return new SimpleDateFormat("yyyyMMdd").format(this.recodingDate);
    }

    public void setRecodingDate(Date recodingDate)
    {
        this.recodingDate = recodingDate;
    }

    public double getButtonPress()
    {
        return buttonPress;
    }

    public void setButtonPress(double buttonPress)
    {
        this.buttonPress = buttonPress;
    }

    public double getUserStart()
    {
        return userStart;
    }

    public void setUserStart(double userStart)
    {
        this.userStart = userStart;
    }

    public double getUserEnd()
    {
        return userEnd;
    }

    public void setUserEnd(double userEnd)
    {
        this.userEnd = userEnd;
    }

    public String getReference()
    {
        return reference;
    }

    public void setReference(String reference)
    {
        this.reference = reference;
    }

    public String getHypothesis()
    {
        return hypothesis;
    }

    public void setHypothesis(String hypothesis)
    {
        this.hypothesis = hypothesis;
    }

    public int getHypothesisWER()
    {
        return hypothesisWER;
    }

    public void setHypothesisWER(int hypothesisWER)
    {
        this.hypothesisWER = hypothesisWER;
    }

    public double getTextDisplayStart()
    {
        return textDisplayStart;
    }

    public void setTextDisplayStart(double textDisplayStart)
    {
        this.textDisplayStart = textDisplayStart;
    }

    public double getTextDisplayEnd()
    {
        return textDisplayEnd;
    }

    public void setTextDisplayEnd(double textDisplayEnd)
    {
        this.textDisplayEnd = textDisplayEnd;
    }

    public double getActionStart()
    {
        return actionStart;
    }

    public void setActionStart(double actionStart)
    {
        this.actionStart = actionStart;
    }

    public String getSystemResponse()
    {
        return systemResponse;
    }

    public void setSystemResponse(String systemResponse)
    {
        this.systemResponse = systemResponse;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getSubCategory()
    {
        return subCategory;
    }

    public void setSubCategory(String subCategory)
    {
        this.subCategory = subCategory;
    }

    public int getSuperBadResult()
    {
        return superBadResult;
    }

    public void setSuperBadResult(int superBadResult)
    {
        this.superBadResult = superBadResult;
    }

    public String getResponseVisual()
    {
        return responseVisual;
    }

    public void setResponseVisual(String responseVisual)
    {
        this.responseVisual = responseVisual;
    }

    public String getResponseAudio()
    {
        return responseAudio;
    }

    public void setResponseAudio(String responseAudio)
    {
        this.responseAudio = responseAudio;
    }

    public String getFormat()
    {
        return format;
    }

    public void setFormat(String format)
    {
        this.format = format;
    }

    public String getResult()
    {
        return result;
    }

    public void setResult(String result)
    {
        this.result = result;
    }

    public double getTtsStart()
    {
        return ttsStart;
    }

    public void setTtsStart(double ttsStart)
    {
        this.ttsStart = ttsStart;
    }

    public double getTtsEnd()
    {
        return ttsEnd;
    }

    public void setTtsEnd(double ttsEnd)
    {
        this.ttsEnd = ttsEnd;
    }

    public String getCoder()
    {
        return coder;
    }

    public void setCoder(String coder)
    {
        this.coder = coder;
    }

    public int getInvocationAttempts()
    {
        return invocationAttempts;
    }

    public void setInvocationAttempts(int invocationAttempts)
    {
        this.invocationAttempts = invocationAttempts;
    }

    public double getLightOn()
    {
        return lightOn;
    }

    public void setLightOn(double lightOn)
    {
        this.lightOn = lightOn;
    }

    public double getMusicDuckingOn()
    {
        return musicDuckingOn;
    }

    public void setMusicDuckingOn(double musicDuckingOn)
    {
        this.musicDuckingOn = musicDuckingOn;
    }

    public double getMusicDuckingOff()
    {
        return musicDuckingOff;
    }

    public void setMusicDuckingOff(double musicDuckingOff)
    {
        this.musicDuckingOff = musicDuckingOff;
    }

    public double getLightOff()
    {
        return lightOff;
    }

    public void setLightOff(double lightOff)
    {
        this.lightOff = lightOff;
    }

    public void saveInfoFromAnvilReference(AnvilReference anvilReference)
    {
        this.fileName = anvilReference.getVideoFilename();
        this.subjectId = anvilReference.getSubjectId();
        this.company = anvilReference.getCompany();
        this.sessionId = anvilReference.getSessionId();
        this.buttonPress = anvilReference.getButtonPress();
        this.userStart = anvilReference.getUserStart();
        this.reference = anvilReference.getReference();
        this.hypothesis = anvilReference.getHypothesis();
        this.textDisplayStart = anvilReference.getTextDisplayStarts();
        this.textDisplayEnd = anvilReference.getTextDisplayEnds();
        this.actionStart = anvilReference.getActionStart();
        this.systemResponse = anvilReference.getSystemResponse();
        this.category = anvilReference.getCategory();
        // TODO: subcategory:
        this.superBadResult = anvilReference.getSuperBadResult();
        this.format = anvilReference.getFormat();
        this.responseVisual = anvilReference.getResponseVisual();
        this.responseAudio = anvilReference.getResponseAudio();
        this.format = anvilReference.getFormat();
        this.result = anvilReference.getResult();
        this.ttsStart = anvilReference.getTtsStart();
        this.ttsEnd = anvilReference.getTtsEnd();
        this.coder = anvilReference.getCoderName();
    }

    public void saveInfoFromPrompt(Prompt promptObj)
    {
        this.prompt = promptObj.getPrompt();
        this.domain = promptObj.getDomain();
        this.position = promptObj.getPosition();
        this.bargeIn = promptObj.getBargeIn();
    }

    public void saveInfoFromSatisfaction(Satisfaction satisfaction)
    {
        this.relevancyGrading = satisfaction.getGrading();
        // TODO: is it from satisfaction file or anvil file?
        this.recodingDate = satisfaction.getRecordDate();
    }
}
