package org.gs4tr.dataforce.datacrunch.tool.service.excel;

import java.util.ArrayList;
import java.util.List;

public class ExcelHeader
{
    public static List<String> phoneHeaders = new ArrayList<>();
    public static List<String> speakerHeaders = new ArrayList<>();

    static
    {
        phoneHeaders.add("File Name");
        phoneHeaders.add("Subject");
        phoneHeaders.add("Country");
        phoneHeaders.add("Company");
        phoneHeaders.add("Device");
        phoneHeaders.add("Session");
        phoneHeaders.add("Prompt");
        phoneHeaders.add("Domain");
        phoneHeaders.add("Relevancy Grading");
        phoneHeaders.add("Recoding Date");
        phoneHeaders.add("[ButtonPress]");
        phoneHeaders.add("[UserStart]");
        phoneHeaders.add("[UserEnd]");
        phoneHeaders.add("[Reference]");
        phoneHeaders.add("[Hypothesis]");
        phoneHeaders.add("[Hypothesis WER]");
        phoneHeaders.add("[SpeechError]");
        phoneHeaders.add("[TextDisplayStart]");
        phoneHeaders.add("[TextDisplayEnds]");
        phoneHeaders.add("[ActionStart]");
        phoneHeaders.add("[SystemResponse]");
        phoneHeaders.add("[Category]");
        phoneHeaders.add("[SubCategory]");
        phoneHeaders.add("[SuperBadResult]");
        phoneHeaders.add("[ResponseVisual]");
        phoneHeaders.add("[ResponseAudio]");
        phoneHeaders.add("[Format]");
        phoneHeaders.add("[Result]");
        phoneHeaders.add("[OtherComments]");
        phoneHeaders.add("[TTSStart]");
        phoneHeaders.add("[TTSEnd]");
        phoneHeaders.add("Coder");

        speakerHeaders.add("File Name");
        speakerHeaders.add("Subject");
        speakerHeaders.add("Country");
        speakerHeaders.add("Company");
        speakerHeaders.add("Device");
        speakerHeaders.add("Session");
        speakerHeaders.add("Prompt");
        speakerHeaders.add("Domain");
        speakerHeaders.add("Position");
        speakerHeaders.add("BargeIn");
        speakerHeaders.add("Relevancy Grading");
        speakerHeaders.add("Recording Date");
        speakerHeaders.add("[Invocation Attempts]");
        speakerHeaders.add("[LightOn]");
        speakerHeaders.add("[UserStart]");
        speakerHeaders.add("[UserEnd]");
        speakerHeaders.add("[Reference]");
        speakerHeaders.add("[Hypothesis]");
        speakerHeaders.add("[Hypothesis WER]");
        speakerHeaders.add("[SpeechError]");
        speakerHeaders.add("[MusicDuckingOn]");
        speakerHeaders.add("[MusicDuckingOff]");
        speakerHeaders.add("[LightOff]");
        speakerHeaders.add("[SystemResponse]");
        speakerHeaders.add("[Category]");
        speakerHeaders.add("[SuperBadResult]");
        speakerHeaders.add("[Response]");
        speakerHeaders.add("[Result]");
        speakerHeaders.add("[OtherComment]");
        speakerHeaders.add("[TTStart]");
        speakerHeaders.add("[TTSEnd]");
        speakerHeaders.add("Coder");
    }
}
