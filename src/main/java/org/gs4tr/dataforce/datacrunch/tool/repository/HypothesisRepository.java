package org.gs4tr.dataforce.datacrunch.tool.repository;

import java.util.List;

import org.gs4tr.dataforce.datacrunch.tool.model.Hypothesis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("hypothesisRepository")
public interface HypothesisRepository extends JpaRepository<Hypothesis, Long>
{
    public List<Hypothesis> findByCompany(String company);
}
