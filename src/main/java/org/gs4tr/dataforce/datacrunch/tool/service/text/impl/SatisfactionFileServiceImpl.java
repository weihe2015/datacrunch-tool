package org.gs4tr.dataforce.datacrunch.tool.service.text.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.gs4tr.dataforce.datacrunch.tool.Messages;
import org.gs4tr.dataforce.datacrunch.tool.model.Satisfaction;
import org.gs4tr.dataforce.datacrunch.tool.repository.SatisfactionRepository;
import org.gs4tr.dataforce.datacrunch.tool.service.file.FilenameService;
import org.gs4tr.dataforce.datacrunch.tool.service.string.StringHelper;
import org.gs4tr.dataforce.datacrunch.tool.service.text.HypothesisFileService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.SatisfactionFileService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.TextFileReaderService;
import org.springframework.beans.factory.annotation.Autowired;

public class SatisfactionFileServiceImpl implements SatisfactionFileService
{
    @Autowired
    private TextFileReaderService textFileReaderService;

    @Autowired
    private SatisfactionRepository satisfactionRepository;

    @Autowired
    private FilenameService filenameService;

    @Override
    public void processSatisfactionFiles(List<String> satisfactionFiles)
    {
        if (satisfactionFiles == null || satisfactionFiles.isEmpty())
        {
            LOG.warn(Messages.getString("SatisfactionService.1"));
            return;
        }

        for (String satisfactionFile : satisfactionFiles)
        {
            processSatisfactionFile(satisfactionFile);
        }
    }

    @Override
    public void processSatisfactionFile(String satisfactionFile)
    {
        String filename = FilenameUtils.getName(satisfactionFile);
        LOG.info(String.format(Messages.getString("SatisfactionService.4"), filename));

        // get subjectId and sessionId from filename
        int subjectId = -1;
        int sessionId = -1;
        Map<String, String> infoMap = filenameService.getInfoMapFromSatisfactionFileName(satisfactionFile);
        if (infoMap.containsKey(FilenameService.SUBJECT_ID))
        {
            String subjectIdText = infoMap.get(FilenameService.SUBJECT_ID);
            subjectId = Integer.parseInt(subjectIdText);
        }
        else
        {
            LOG.warn(String.format(Messages.getString("SatisfactionService.7"), filename));
        }

        if (infoMap.containsKey(FilenameService.SESSION_ID))
        {
            String sessionIdText = infoMap.get(FilenameService.SESSION_ID);
            sessionId = Integer.parseInt(sessionIdText);
        }
        else
        {
            LOG.warn(String.format(Messages.getString("SatisfactionService.8"), filename));
        }

        List<String> contents = textFileReaderService.getContents(satisfactionFile);
        for (String content : contents)
        {
            processSatisfactionContent(filename, content, subjectId, sessionId);
        }
    }

    private void processSatisfactionContent(String satisfactionFile, String content, int subjectId, int sessionId)
    {
        if (StringUtils.isBlank(content))
        {
            return;
        }
        Satisfaction satisfaction = new Satisfaction();
        // split the content by comma:
        String[] splitted = content.split(HypothesisFileService.CSV_SPLITTED_WITH_QUOTE);

        if (splitted.length != 3)
        {
            LOG.warn(String.format(Messages.getString("SatisfactionService.5"), satisfactionFile, content));
            return;
        }

        Date date;
        try
        {
            date = new SimpleDateFormat(StringHelper.DATE_SPECIFIC_FORMAT).parse(splitted[0]);
            satisfaction.setRecordDate(date);
        }
        catch (ParseException ex)
        {
            LOG.warn(
                String.format(Messages.getString("SatisfactionService.3"), content, satisfactionFile, ex.getMessage()));
        }

        try
        {
            satisfaction.setPromptId(Integer.parseInt(splitted[1]));
            satisfaction.setGrading(splitted[2]);
            satisfaction.setSubjectId(subjectId);
            satisfaction.setSessionId(sessionId);

            // save satisfaction to database:
            satisfactionRepository.save(satisfaction);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("SatisfactionService.6"), satisfactionFile, ex.getMessage()));
        }
    }

}
