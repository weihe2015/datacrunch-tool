package org.gs4tr.dataforce.datacrunch.tool.service.xml;

import org.dom4j.Document;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("XmlReaderService")
public interface XmlReaderService
{
    public static Logger LOG = LoggerFactory.getLogger(XmlReaderService.class);

    public Document getDocument(String fileName);

    public Element getRootElement(Document doc);

    public String getVideoFilename(Element root);

    public String getCoderName(Element root);

    public double getButtonPress(Element root);

    public double getActionStart(Element root);

    public double getUserStart(Element root);

    public double getUserEnd(Element root);

    public String getReference(Element root);

    public String getHypothesis(Element root);

    public double getTextDisplayStarts(Element root);

    public double getTextDisplayEnds(Element root);

    public String getSystemResponse(Element root);

    public String getCategory(Element root);

    public int getSuperBadResult(Element root);

    public String getResponseAudio(Element root);

    public String getResult(Element root);

    public double getTTSStart(Element root);

    public double getTTSEnd(Element root);

    public double getLightOn(Element root);

    public double getLightOff(Element root);

    public int getInvocationAttempts(Element root);

    public double getMusicDuckingOn(Element root);

    public double getMusicDuckingOff(Element root);

    public String getFormat(Element root);

    public String getResponseVisual(Element root);
}
