package org.gs4tr.dataforce.datacrunch.tool.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.json.JSONObject;

@Entity
@Table(name = "prompt")
public class Prompt implements Serializable
{
    private static final long serialVersionUID = -1281609985594057893L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "subjectId", nullable = false)
    private int subjectId;

    // Line number is prompt Id
    @Column(name = "promptId", nullable = false)
    private int promptId;

    @Column(name = "prompt", nullable = false)
    private String prompt;

    @Column(name = "domain", nullable = false)
    private String domain;

    @Column(name = "position", nullable = false)
    private String position;

    // Some prompt may not have bargeIn field empty
    @Column(name = "BargeIn", nullable = true)
    private String bargeIn;

    public Prompt()
    {

    }

    public int getSubjectId()
    {
        return subjectId;
    }

    public void setSubjectId(int subjectId)
    {
        this.subjectId = subjectId;
    }

    public int getPromptId()
    {
        return promptId;
    }

    public void setPromptId(int promptId)
    {
        this.promptId = promptId;
    }

    public String getPrompt()
    {
        return prompt;
    }

    public void setPrompt(String prompt)
    {
        this.prompt = prompt;
    }

    public String getDomain()
    {
        return domain;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public String getPosition()
    {
        return position;
    }

    public void setPosition(String position)
    {
        this.position = position;
    }

    public String getBargeIn()
    {
        return bargeIn;
    }

    public void setBargeIn(String bargeIn)
    {
        this.bargeIn = bargeIn;
    }

    @Override
    public String toString()
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Id", this.id);
        jsonObject.put("subjectId", this.subjectId);
        jsonObject.put("promptId", this.promptId);
        jsonObject.put("prompt", this.prompt);
        jsonObject.put("domain", this.domain);
        jsonObject.put("position", position);
        jsonObject.put("bargeIn", this.bargeIn);
        return jsonObject.toString(2);
    }
}
