package org.gs4tr.dataforce.datacrunch.tool.service.text;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("TextFileReaderService")
public interface TextFileReaderService
{
    public static Logger LOG = LoggerFactory.getLogger(TextFileReaderService.class);

    public List<String> getContents(String fileName);

}
