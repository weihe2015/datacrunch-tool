package org.gs4tr.dataforce.datacrunch.tool.service.text;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("PromptFileService")
public interface PromptFileService
{
    public static Logger LOG = LoggerFactory.getLogger(PromptFileService.class);

    public void processPromptFiles(List<String> promptFiles);

    public void processPromptFile(String promptFile);
}
