package org.gs4tr.dataforce.datacrunch.tool.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.gs4tr.dataforce.datacrunch.tool.service.string.StringHelper;
import org.json.JSONObject;

@Entity
@Table(name = "satisfaction")
public class Satisfaction implements Serializable
{
    private static final long serialVersionUID = -8610139027715912814L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "sessionId", nullable = false)
    private int sessionId;

    @Column(name = "subjectId", nullable = false)
    private int subjectId;

    @Column(name = "recordDate", nullable = false)
    private Date recordDate;

    @Column(name = "promptId", nullable = false)
    private int promptId;

    @Column(name = "grading", nullable = false)
    private String grading;

    public Satisfaction()
    {

    }

    public int getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(int sessionId)
    {
        this.sessionId = sessionId;
    }

    public int getSubjectId()
    {
        return subjectId;
    }

    public void setSubjectId(int subjectId)
    {
        this.subjectId = subjectId;
    }

    public Date getRecordDate()
    {
        return recordDate;
    }

    public void setRecordDate(Date recordDate)
    {
        this.recordDate = recordDate;
    }

    public int getPromptId()
    {
        return promptId;
    }

    public void setPromptId(int promptId)
    {
        this.promptId = promptId;
    }

    public String getGrading()
    {
        return grading;
    }

    public void setGrading(String grading)
    {
        this.grading = grading;
    }

    @Override
    public String toString()
    {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("Id", this.id);
        jsonObject.put("sessionId", this.sessionId);
        jsonObject.put("subjectId", this.subjectId);
        jsonObject.put("recordDate", new SimpleDateFormat(StringHelper.DATE_SPECIFIC_FORMAT).format(this.recordDate));
        jsonObject.put("promptId", this.promptId);
        jsonObject.put("grading", this.grading);
        return jsonObject.toString(2);
    }
}
