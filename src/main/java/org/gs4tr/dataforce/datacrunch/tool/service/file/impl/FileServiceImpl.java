package org.gs4tr.dataforce.datacrunch.tool.service.file.impl;

import java.io.File;
import java.util.List;

import org.gs4tr.dataforce.datacrunch.tool.service.file.FileService;

public class FileServiceImpl implements FileService
{
    @Override
    public void getFiles(String folderName, List<String> anvilFiles, List<String> hypothesisFiles,
        List<String> promptFiles, List<String> satisfactionFiles)
    {
        File folder = new File(folderName);
        if (folder.listFiles() == null || folder.listFiles().length == 0)
        {
            return;
        }

        for (File file : folder.listFiles())
        {
            if (file.isDirectory())
            {
                getFiles(file.getAbsolutePath(), anvilFiles, hypothesisFiles, promptFiles, satisfactionFiles);
            }
            else if (file.isFile())
            {
                String fileName = file.getName();
                String parFolder = file.getParent();

                if (fileName.endsWith(ANVIL_EXTENSION))
                {
                    anvilFiles.add(file.getAbsolutePath());
                }
                else if (fileName.endsWith(TXT_EXTENSION) &&
                    (parFolder.endsWith(SATISFACTION) || parFolder.endsWith(SATISFACTION2) ||
                        parFolder.endsWith(SATISFACTION3)))
                {
                    satisfactionFiles.add(file.getAbsolutePath());
                }
                else if (fileName.endsWith(CSV_EXTENSION) || fileName.endsWith(HTML_EXTENSION))
                {
                    if (parFolder.endsWith(HYPOTHESIS) || parFolder.endsWith(HYPOTHESIS2) ||
                        parFolder.endsWith(HYPOTHESIS3))
                    {
                        hypothesisFiles.add(file.getAbsolutePath());
                    }
                    else if (parFolder.endsWith(PROMPT) || parFolder.endsWith(PROMPT2) ||
                        parFolder.endsWith(PROMPT3))
                    {
                        promptFiles.add(file.getAbsolutePath());
                    }
                }
            }
        }
    }

}
