package org.gs4tr.dataforce.datacrunch.tool.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.json.JSONObject;

@Entity
@Table(name = "anvil_reference")
public class AnvilReference implements Serializable
{
    private static final long serialVersionUID = -826648620009312178L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "fileName", nullable = false)
    private String fileName;

    @Column(name = "videoFilename", nullable = false)
    private String videoFilename;

    @Column(name = "subjectId", nullable = false)
    private int subjectId = -1;

    @Column(name = "country", nullable = true)
    private String country;

    @Column(name = "company", nullable = true)
    private String company;

    @Column(name = "device", nullable = true)
    private String device;

    @Column(name = "sessionId", nullable = false)
    private int sessionId = -1;

    @Column(name = "promptId", nullable = false)
    private int promptId = -1;

    @Column(name = "coderName", nullable = true)
    private String coderName;

    @Column(name = "buttonPress", nullable = true)
    private double buttonPress;

    @Column(name = "userStart", nullable = true)
    private double userStart;

    @Column(name = "userEnd", nullable = true)
    private double userEnd;

    @Column(name = "lightOn", nullable = true)
    private double lightOn;

    @Column(name = "lightOff", nullable = true)
    private double lightOff;

    @Column(name = "musicDuckingOn", nullable = true)
    private double musicDuckingOn;

    @Column(name = "musicDuckingOff", nullable = true)
    private double musicDuckingOff;

    @Column(name = "invocationAttempts", nullable = true)
    private int invocationAttempts = 1;

    @Column(name = "reference", nullable = true)
    private String reference;

    // Only anvil files from phone device contains hypothesis info
    // For speaker device, hypothesis may need to gather from csv files
    @Column(name = "hypothesis", nullable = true)
    private String hypothesis;

    @Column(name = "textDisplayStarts", nullable = true)
    private double textDisplayStarts;

    @Column(name = "textDisplayEnds", nullable = true)
    private double textDisplayEnds;

    @Column(name = "actionStart", nullable = true)
    private double actionStart;

    @Column(name = "systemResponse", nullable = true)
    private String systemResponse;

    @Column(name = "category", nullable = true)
    private String category;

    @Column(name = "superBadResult", nullable = true)
    private int superBadResult;

    @Column(name = "responseAudio", nullable = true, columnDefinition = "LONGTEXT")
    private String responseAudio;

    @Column(name = "textDisplay", nullable = true)
    private int textDisplay;

    @Column(name = "responseVisual", nullable = true)
    private String responseVisual;

    @Column(name = "format", nullable = true)
    private String format;

    @Column(name = "result", nullable = true)
    private String result;

    @Column(name = "ttsStart", nullable = true)
    private double ttsStart;

    @Column(name = "ttsEnd", nullable = true)
    private double ttsEnd;

    public AnvilReference()
    {

    }

    public String getFileName()
    {
        return fileName;
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public String getVideoFilename()
    {
        return videoFilename;
    }

    public void setVideoFilename(String videoFilename)
    {
        this.videoFilename = videoFilename;
    }

    public int getSubjectId()
    {
        return subjectId;
    }

    public void setSubjectId(int subjectId)
    {
        this.subjectId = subjectId;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getCompany()
    {
        return company;
    }

    public void setCompany(String company)
    {
        this.company = company;
    }

    public String getDevice()
    {
        return device;
    }

    public void setDevice(String device)
    {
        this.device = device;
    }

    public int getSessionId()
    {
        return sessionId;
    }

    public void setSessionId(int sessionId)
    {
        this.sessionId = sessionId;
    }

    public int getPromptId()
    {
        return promptId;
    }

    public void setPromptId(int promptId)
    {
        this.promptId = promptId;
    }

    public String getCoderName()
    {
        return coderName;
    }

    public void setCoderName(String coderName)
    {
        this.coderName = coderName;
    }

    public double getButtonPress()
    {
        return buttonPress;
    }

    public void setButtonPress(double buttonPress)
    {
        this.buttonPress = buttonPress;
    }

    public double getUserStart()
    {
        return userStart;
    }

    public void setUserStart(double userStart)
    {
        this.userStart = userStart;
    }

    public double getUserEnd()
    {
        return userEnd;
    }

    public void setUserEnd(double userEnd)
    {
        this.userEnd = userEnd;
    }

    public double getLightOn()
    {
        return lightOn;
    }

    public void setLightOn(double lightOn)
    {
        this.lightOn = lightOn;
    }

    public double getLightOff()
    {
        return lightOff;
    }

    public void setLightOff(double lightOff)
    {
        this.lightOff = lightOff;
    }

    public double getMusicDuckingOn()
    {
        return musicDuckingOn;
    }

    public void setMusicDuckingOn(double musicDuckingOn)
    {
        this.musicDuckingOn = musicDuckingOn;
    }

    public double getMusicDuckingOff()
    {
        return musicDuckingOff;
    }

    public void setMusicDuckingOff(double musicDuckingOff)
    {
        this.musicDuckingOff = musicDuckingOff;
    }

    public int getInvocationAttempts()
    {
        return invocationAttempts;
    }

    public void setInvocationAttempts(int invocationAttempts)
    {
        this.invocationAttempts = invocationAttempts;
    }

    public String getReference()
    {
        return reference;
    }

    public void setReference(String reference)
    {
        this.reference = reference;
    }

    public String getHypothesis()
    {
        return hypothesis;
    }

    public void setHypothesis(String hypothesis)
    {
        this.hypothesis = hypothesis;
    }

    public double getTextDisplayEnds()
    {
        return textDisplayEnds;
    }

    public double getTextDisplayStarts()
    {
        return textDisplayStarts;
    }

    public void setTextDisplayStarts(double textDisplayStarts)
    {
        this.textDisplayStarts = textDisplayStarts;
    }

    public void setTextDisplayEnds(double textDisplayEnds)
    {
        this.textDisplayEnds = textDisplayEnds;
    }

    public double getActionStart()
    {
        return actionStart;
    }

    public void setActionStart(double actionStart)
    {
        this.actionStart = actionStart;
    }

    public String getSystemResponse()
    {
        return systemResponse;
    }

    public void setSystemResponse(String systemResponse)
    {
        this.systemResponse = systemResponse;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public int getSuperBadResult()
    {
        return superBadResult;
    }

    public void setSuperBadResult(int superBadResult)
    {
        this.superBadResult = superBadResult;
    }

    public String getResponseAudio()
    {
        return responseAudio;
    }

    public void setResponseAudio(String responseAudio)
    {
        this.responseAudio = responseAudio;
    }

    public int getTextDisplay()
    {
        return textDisplay;
    }

    public void setTextDisplay(int textDisplay)
    {
        this.textDisplay = textDisplay;
    }

    public String getResponseVisual()
    {
        return responseVisual;
    }

    public void setResponseVisual(String responseVisual)
    {
        this.responseVisual = responseVisual;
    }

    public String getFormat()
    {
        return format;
    }

    public void setFormat(String format)
    {
        this.format = format;
    }

    public String getResult()
    {
        return result;
    }

    public void setResult(String result)
    {
        this.result = result;
    }

    public double getTtsStart()
    {
        return ttsStart;
    }

    public void setTtsStart(double ttsStart)
    {
        this.ttsStart = ttsStart;
    }

    public double getTtsEnd()
    {
        return ttsEnd;
    }

    public void setTtsEnd(double ttsEnd)
    {
        this.ttsEnd = ttsEnd;
    }

    @Override
    public String toString()
    {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("Id", this.id);
        jsonObj.put("filename", this.fileName);
        jsonObj.put("country", this.country);
        jsonObj.put("company", this.company);
        jsonObj.put("device", this.device);
        jsonObj.put("sessionId", this.sessionId);
        jsonObj.put("promptId", this.promptId);
        jsonObj.put("reference", this.reference);
        return jsonObj.toString(2);
    }
}
