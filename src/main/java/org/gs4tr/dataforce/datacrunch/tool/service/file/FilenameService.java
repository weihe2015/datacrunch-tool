package org.gs4tr.dataforce.datacrunch.tool.service.file;

import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

@Service("FilenameService")
public interface FilenameService
{
    static public final String SUBJECT_ID = "subjectId";
    static public final String COMPANY = "company";
    static public final String SESSION_ID = "sessionId";
    static public final String PROMPT_ID = "promptId";

    static public final Pattern anvilFileNamePattern = Pattern.compile(
        "s(\\d+)[_-]([A-Za-z]+)[_-]session[_-]?(\\d+)[_-](pr)?(\\d+)",
        Pattern.CASE_INSENSITIVE);

    static public final Pattern satisfactionFileNamePattern = Pattern.compile("_Subject(\\d+)_Session(\\d+)",
        Pattern.CASE_INSENSITIVE);

    static public final Pattern promptFileNamePattern = Pattern.compile("-Subject-(\\d+)\\.csv$",
        Pattern.CASE_INSENSITIVE);

    static public final Pattern hypothesisFileNamePattern = Pattern.compile("(Amazon|Apple|Google).*\\.csv",
        Pattern.CASE_INSENSITIVE);

    public Map<String, String> getInfoMapFromAnvilFileName(String anvilFileName);

    public Map<String, String> getInfoMapFromSatisfactionFileName(String satisfactionFileName);

    public Map<String, String> getInfoMapFromPromptFileName(String promptFileName);

    public Map<String, String> getInfoMapFromHypothesisFileName(String hypothesisFileName);
}
