package org.gs4tr.dataforce.datacrunch.tool.service.xml.impl;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.gs4tr.dataforce.datacrunch.tool.Messages;
import org.gs4tr.dataforce.datacrunch.tool.model.AnvilReference;
import org.gs4tr.dataforce.datacrunch.tool.repository.AnvilReferenceRepository;
import org.gs4tr.dataforce.datacrunch.tool.service.file.FilenameService;
import org.gs4tr.dataforce.datacrunch.tool.service.xml.AnvilService;
import org.gs4tr.dataforce.datacrunch.tool.service.xml.XmlReaderService;
import org.springframework.beans.factory.annotation.Autowired;

public class AnvilServiceImpl implements AnvilService
{
    @Autowired
    private XmlReaderService xmlReaderService;

    @Autowired
    private FilenameService filenameService;

    @Autowired
    private AnvilReferenceRepository anvilReferenceRepository;

    @Override
    public void processAnvilFiles(List<String> anvilFiles)
    {
        if (anvilFiles == null || anvilFiles.isEmpty())
        {
            return;
        }
        for (String anvilFile : anvilFiles)
        {
            String fileName = FilenameUtils.getName(anvilFile);
            if (!new File(anvilFile).exists())
            {
                LOG.warn(String.format(Messages.getString("AnvilService.1"), fileName));
                continue;
            }
            LOG.info(String.format(Messages.getString("AnvilService.3"), fileName));

            // read anvil file:
            Document doc = xmlReaderService.getDocument(anvilFile);
            if (doc == null)
            {
                LOG.error(String.format(Messages.getString("AnvilService.8"), fileName));
                continue;
            }

            Element root = xmlReaderService.getRootElement(doc);
            // combine these info into AnvilReference Object and save it to db.
            saveAnvilReference(anvilFile, root);
        }
    }

    private void saveAnvilReference(String fileName, Element root)
    {
        if (root == null || !root.hasContent())
        {
            return;
        }
        AnvilReference anvilReference = new AnvilReference();
        anvilReference.setFileName(fileName);

        // Get info from anvil filename
        fileName = FilenameUtils.getName(fileName);
        Map<String, String> infoMap = filenameService.getInfoMapFromAnvilFileName(fileName);
        // Subject Id:
        if (infoMap.containsKey(FilenameService.SUBJECT_ID))
        {
            String subjectIdText = infoMap.get(FilenameService.SUBJECT_ID);
            anvilReference.setSubjectId(Integer.parseInt(subjectIdText));
        }
        else
        {
            LOG.warn(String.format(Messages.getString("AnvilService.4"), fileName));
        }

        // set country as USA unless it is changed.
        anvilReference.setCountry("USA");
        // company name:
        if (infoMap.containsKey(FilenameService.COMPANY))
        {
            String company = infoMap.get(FilenameService.COMPANY);
            anvilReference.setCompany(company);
            anvilReference.setDevice(getDevice(company));
        }
        else
        {
            LOG.warn(String.format(Messages.getString("AnvilService.5"), fileName));
        }

        // session Id:
        if (infoMap.containsKey(FilenameService.SESSION_ID))
        {
            String sessionIdText = infoMap.get(FilenameService.SESSION_ID);
            anvilReference.setSessionId(Integer.parseInt(sessionIdText));
        }
        else
        {
            LOG.warn(String.format(Messages.getString("AnvilService.6"), fileName));
        }

        // prompt Id:
        if (infoMap.containsKey(FilenameService.PROMPT_ID))
        {
            String promptIdText = infoMap.get(FilenameService.PROMPT_ID);
            anvilReference.setPromptId(Integer.parseInt(promptIdText));
        }
        else
        {
            LOG.warn(String.format(Messages.getString("AnvilService.7"), fileName));
        }

        // video file name:
        String videoFilename = xmlReaderService.getVideoFilename(root);
        videoFilename = FilenameUtils.getName(videoFilename);
        anvilReference.setVideoFilename(videoFilename);

        anvilReference.setCoderName(xmlReaderService.getCoderName(root));
        anvilReference.setButtonPress(xmlReaderService.getButtonPress(root));
        anvilReference.setActionStart(xmlReaderService.getActionStart(root));
        anvilReference.setUserStart(xmlReaderService.getUserStart(root));
        anvilReference.setUserEnd(xmlReaderService.getUserEnd(root));
        anvilReference.setLightOn(xmlReaderService.getLightOn(root));
        anvilReference.setLightOff(xmlReaderService.getLightOff(root));
        anvilReference.setMusicDuckingOn(xmlReaderService.getMusicDuckingOn(root));
        anvilReference.setMusicDuckingOff(xmlReaderService.getMusicDuckingOff(root));
        anvilReference.setInvocationAttempts(xmlReaderService.getInvocationAttempts(root));
        anvilReference.setReference(xmlReaderService.getReference(root));
        anvilReference.setHypothesis(xmlReaderService.getHypothesis(root));
        anvilReference.setTextDisplayStarts(xmlReaderService.getTextDisplayStarts(root));
        anvilReference.setTextDisplayEnds(xmlReaderService.getTextDisplayEnds(root));
        anvilReference.setFormat(xmlReaderService.getFormat(root));
        anvilReference.setResponseVisual(xmlReaderService.getResponseVisual(root));
        anvilReference.setSystemResponse(xmlReaderService.getSystemResponse(root));
        anvilReference.setCategory(xmlReaderService.getCategory(root));
        anvilReference.setSuperBadResult(xmlReaderService.getSuperBadResult(root));
        anvilReference.setResponseAudio(xmlReaderService.getResponseAudio(root));
        anvilReference.setResult(xmlReaderService.getResult(root));
        anvilReference.setTtsStart(xmlReaderService.getTTSStart(root));
        anvilReference.setTtsEnd(xmlReaderService.getTTSEnd(root));

        // save to database
        anvilReferenceRepository.save(anvilReference);
    }

    private String getDevice(String company)
    {
        company = company.toLowerCase().trim();
        switch (company)
        {
        case "amazon":
            return "Echo";
        case "apple":
            return "HomePod";
        case "google":
            return "Google Home";
        default:
            return "N/A";
        }
    }
}
