package org.gs4tr.dataforce.datacrunch.tool.service.xml;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("anvilService")
public interface AnvilService
{
    public static Logger LOG = LoggerFactory.getLogger(AnvilService.class);

    public void processAnvilFiles(List<String> anvilFiles);
}
