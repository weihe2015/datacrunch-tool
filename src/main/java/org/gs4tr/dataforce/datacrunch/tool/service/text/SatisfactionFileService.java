package org.gs4tr.dataforce.datacrunch.tool.service.text;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("SatisfactionFileService")
public interface SatisfactionFileService
{
    public static Logger LOG = LoggerFactory.getLogger(SatisfactionFileService.class);

    public void processSatisfactionFiles(List<String> satisfactionFiles);

    public void processSatisfactionFile(String satisfactionFile);
}
