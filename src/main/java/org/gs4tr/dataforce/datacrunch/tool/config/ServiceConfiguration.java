package org.gs4tr.dataforce.datacrunch.tool.config;

import org.gs4tr.dataforce.datacrunch.tool.service.excel.ExcelWriterService;
import org.gs4tr.dataforce.datacrunch.tool.service.excel.impl.ExcelWriterServiceImpl;
import org.gs4tr.dataforce.datacrunch.tool.service.file.FileService;
import org.gs4tr.dataforce.datacrunch.tool.service.file.FilenameService;
import org.gs4tr.dataforce.datacrunch.tool.service.file.impl.FileServiceImpl;
import org.gs4tr.dataforce.datacrunch.tool.service.file.impl.FilenameServiceImpl;
import org.gs4tr.dataforce.datacrunch.tool.service.html.HtmlReaderService;
import org.gs4tr.dataforce.datacrunch.tool.service.html.impl.HtmlReaderServiceImpl;
import org.gs4tr.dataforce.datacrunch.tool.service.text.HypothesisFileService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.PromptFileService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.SatisfactionFileService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.TextFileReaderService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.impl.HypothesisFileServiceImpl;
import org.gs4tr.dataforce.datacrunch.tool.service.text.impl.PromptFileServiceImpl;
import org.gs4tr.dataforce.datacrunch.tool.service.text.impl.SatisfactionFileServiceImpl;
import org.gs4tr.dataforce.datacrunch.tool.service.text.impl.TextFileReaderServiceImpl;
import org.gs4tr.dataforce.datacrunch.tool.service.xml.AnvilService;
import org.gs4tr.dataforce.datacrunch.tool.service.xml.XmlReaderService;
import org.gs4tr.dataforce.datacrunch.tool.service.xml.impl.AnvilServiceImpl;
import org.gs4tr.dataforce.datacrunch.tool.service.xml.impl.XmlReaderServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration
{
    @Bean
    public FileService fileService()
    {
        return new FileServiceImpl();
    }

    @Bean
    public FilenameService fileNameService()
    {
        return new FilenameServiceImpl();
    }

    @Bean
    public HypothesisFileService hypothesisFileService()
    {
        return new HypothesisFileServiceImpl();
    }

    @Bean
    public PromptFileService promptFileService()
    {
        return new PromptFileServiceImpl();
    }

    @Bean
    public SatisfactionFileService satisfactionFileService()
    {
        return new SatisfactionFileServiceImpl();
    }

    @Bean
    public TextFileReaderService textFileReaderService()
    {
        return new TextFileReaderServiceImpl();
    }

    @Bean
    public AnvilService anvilService()
    {
        return new AnvilServiceImpl();
    }

    @Bean
    public XmlReaderService xmlReaderService()
    {
        return new XmlReaderServiceImpl();
    }

    @Bean
    public ExcelWriterService excelWriterService()
    {
        return new ExcelWriterServiceImpl();
    }

    @Bean
    public HtmlReaderService htmlReaderService()
    {
        return new HtmlReaderServiceImpl();
    }
}
