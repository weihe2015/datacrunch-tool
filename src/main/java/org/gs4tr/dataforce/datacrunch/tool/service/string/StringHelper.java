package org.gs4tr.dataforce.datacrunch.tool.service.string;

public class StringHelper
{
    static public final String PHONE = "phone";
    static public final String SPEAKER = "speaker";
    static public final String DATEFORMATE = "yyyy-MM-dd";
    static public final String DATE_SPECIFIC_FORMAT = "yyyyMMdd HH:mm:ss";
    static public final String DATE_SPECIFIC_FORMAT2 = "yyyyMMdd_HH_mm_ss";

    /**
     * Remove starting and ending quote from string: Ex: "Hey Siri set an alarm for today and tomorrow"
     * output: Hey Siri set an alarm for today and tomorrow
     * 
     * @param input String: read from hypothesis file
     * @return output String: removed quoted
     * @exception: null
     */
    public static String removeQuote(String str)
    {
        StringBuffer sb = new StringBuffer();
        for (int i = 0, max = str.length(); i < max; i++)
        {
            char c = str.charAt(i);
            if ((i == 0 || i == max - 1) && c == '\"')
            {
                continue;
            }
            else
            {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String fixInvalidXmlChars(String s)
    {
        StringBuffer sb = new StringBuffer(s.length());
        for (int i = 0, max = s.length(); i < max; i++)
        {
            char ch = s.charAt(i);
            if (ch == 9 || ch == 10 || ch == 13 || ch >= 32)
            {
                sb.append(ch);
            }
        }
        return sb.toString();
    }
}
