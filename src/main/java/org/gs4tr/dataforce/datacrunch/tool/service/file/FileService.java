package org.gs4tr.dataforce.datacrunch.tool.service.file;

import java.util.List;

import org.springframework.stereotype.Service;

@Service("FileService")
public interface FileService
{
    static public String CSV_EXTENSION = ".csv";
    static public String ANVIL_EXTENSION = ".anvil";
    static public String ANVIL_BACKUP_EXTENSION = ".anvil.backup";
    static public String TXT_EXTENSION = ".txt";
    static public String HTML_EXTENSION = ".html";
    static public String XLSX_EXTENSION = ".xlsx";

    static public String HYPOTHESIS = "Hypothesis";
    static public String HYPOTHESIS2 = "hypothesis";
    static public String HYPOTHESIS3 = "HYPOTHESIS";

    static public String PROMPT = "Prompts";
    static public String PROMPT2 = "prompts";
    static public String PROMPT3 = "PROMPT";

    static public String SATISFACTION = "Satisfaction";
    static public String SATISFACTION2 = "satisfaction";
    static public String SATISFACTION3 = "SATISFACTION";

    public void getFiles(String folderName, List<String> anvilFiles, List<String> hypothesisFiles,
        List<String> promptFiles, List<String> satisfactionFiles);

}
