package org.gs4tr.dataforce.datacrunch.tool.repository;

import org.gs4tr.dataforce.datacrunch.tool.model.AnvilReference;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("anvilReferenceRepository")
public interface AnvilReferenceRepository extends JpaRepository<AnvilReference, Long>
{

}
