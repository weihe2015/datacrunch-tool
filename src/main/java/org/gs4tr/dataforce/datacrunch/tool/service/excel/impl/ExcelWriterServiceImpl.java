package org.gs4tr.dataforce.datacrunch.tool.service.excel.impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.gs4tr.dataforce.datacrunch.tool.model.ExcelInfo;
import org.gs4tr.dataforce.datacrunch.tool.service.excel.ExcelHeader;
import org.gs4tr.dataforce.datacrunch.tool.service.excel.ExcelWriterService;
import org.gs4tr.dataforce.datacrunch.tool.service.string.StringHelper;

public class ExcelWriterServiceImpl implements ExcelWriterService
{

    @Override
    public void write(String outputFileName, List<ExcelInfo> excelInfos, String type) throws IOException
    {
        Workbook workbook = null;
        FileOutputStream outputStream = null;
        try
        {
            // Create a Workbook
            workbook = new XSSFWorkbook();
            /* CreationHelper helps us create instances of various things like DataFormat,
            Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
            CreationHelper createHelper = workbook.getCreationHelper();

            // Create a Sheet
            Sheet sheet = workbook.createSheet("References");
            createHeaderRow(workbook, sheet, type);

            // Create Cell Style for formatting Date
            CellStyle dateCellStyle = workbook.createCellStyle();
            dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

            int rowNum = 1;
            for (ExcelInfo excelInfo : excelInfos)
            {
                if (StringUtils.equalsIgnoreCase(StringHelper.PHONE, type))
                {
                    createPhoneExcelInfoRow(workbook, sheet, excelInfo, rowNum);
                }
                else
                {
                    createSpeakerExcelInfoRow(workbook, sheet, excelInfo, rowNum);
                }
                rowNum++;
            }

            // Write workbook to a file:
            outputStream = new FileOutputStream(outputFileName);
            workbook.write(outputStream);
        }
        finally
        {
            if (outputStream != null)
            {
                outputStream.close();
            }

            // Closing the workbook
            if (workbook != null)
            {
                workbook.close();
            }
        }
    }

    /**
     * For each field in ExcelInfo, set the value into the cell of correct position for Phone Type:
     * 
     * @param Workbook workbook, Sheet sheet, ExcelInfo excelInfo, int rowNum
     */
    private void createPhoneExcelInfoRow(Workbook workbook, Sheet sheet, ExcelInfo excelInfo, int rowNum)
    {
        Row row = sheet.createRow(rowNum);
        CellStyle rowStyle = workbook.createCellStyle();
        Cell cell = null;

        // set filename:
        if (excelInfo.getFileName() != null)
        {
            cell = row.createCell(0);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getFileName());
        }

        // set subject Id:
        if (excelInfo.getSubjectId() != -1)
        {
            cell = row.createCell(1);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(String.format("%03d", excelInfo.getSubjectId()));
        }

        // skip country: column 2
        // set Company:
        if (excelInfo.getCompany() != null)
        {
            cell = row.createCell(3);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getCompany().toUpperCase());
        }

        // skip device, column 4
        // set session:
        if (excelInfo.getSessionId() != -1)
        {
            cell = row.createCell(5);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getSessionId());
        }

        // set prompt:
        if (excelInfo.getPrompt() != null)
        {
            cell = row.createCell(6);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getPrompt());
        }

        // set domain
        if (excelInfo.getDomain() != null)
        {
            cell = row.createCell(7);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getDomain());
        }

        // set relevancy grading
        if (excelInfo.getRelevancyGrading() != null)
        {
            cell = row.createCell(8);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getRelevancyGrading());
        }

        // set recording date:
        if (excelInfo.getRecodingDate() != null)
        {
            cell = row.createCell(9);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getRecordDateInFormat());
        }

        // set button press:
        cell = row.createCell(10);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getButtonPress() != -1)
        {
            cell.setCellValue(excelInfo.getButtonPress());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set user start:
        cell = row.createCell(11);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getUserStart() != -1)
        {
            cell.setCellValue(excelInfo.getUserStart());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set user end:
        cell = row.createCell(12);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getUserEnd() != -1)
        {
            cell.setCellValue(excelInfo.getUserEnd());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set reference:
        if (excelInfo.getReference() != null)
        {
            cell = row.createCell(13);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getReference());
        }

        // set hypothesis:
        if (excelInfo.getHypothesis() != null)
        {
            cell = row.createCell(14);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getHypothesis());
        }

        // skip Hypothesis WER: column 15
        // skip SpeechError: column 16
        // set text display starts:
        cell = row.createCell(17);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getTextDisplayStart() != -1)
        {
            cell.setCellValue(excelInfo.getTextDisplayStart());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set text display ends
        cell = row.createCell(18);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getTextDisplayEnd() != -1)
        {
            cell.setCellValue(excelInfo.getTextDisplayEnd());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set action start:
        cell = row.createCell(19);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getActionStart() != -1)
        {
            cell.setCellValue(excelInfo.getActionStart());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set System Response:
        if (excelInfo.getSystemResponse() != null)
        {
            cell = row.createCell(20);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getSystemResponse());
        }

        // set Category:
        if (excelInfo.getCategory() != null)
        {
            cell = row.createCell(21);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getCategory());
        }

        // set subCategory:
        cell = row.createCell(22);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getSubCategory() != null)
        {
            cell.setCellValue(excelInfo.getSubCategory());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set superBadResult:
        cell = row.createCell(23);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getSuperBadResult() != -1)
        {
            cell.setCellValue(excelInfo.getSuperBadResult());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set Response Visual:
        cell = row.createCell(24);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getResponseVisual() != null)
        {
            cell.setCellValue(excelInfo.getResponseVisual());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set Response Audio
        cell = row.createCell(25);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getResponseAudio() != null)
        {
            cell.setCellValue(excelInfo.getResponseAudio());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set format:
        cell = row.createCell(26);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getFormat() != null)
        {
            cell.setCellValue(excelInfo.getFormat());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set result:
        if (excelInfo.getResult() != null)
        {
            cell = row.createCell(27);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getResult());
        }

        // skip other comment: column 28
        // set TTSStart:
        cell = row.createCell(29);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getTtsStart() != -1)
        {
            cell.setCellValue(excelInfo.getTtsStart());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set TTSEnd
        cell = row.createCell(30);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getTtsEnd() != -1)
        {
            cell.setCellValue(excelInfo.getTtsEnd());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set coder:
        if (excelInfo.getCoder() != null)
        {
            cell = row.createCell(31);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getCoder());
        }
    }

    /**
     * For each field in ExcelInfo, set the value into the cell of correct position for Speaker Type:
     * 
     * @param Workbook workbook, Sheet sheet, ExcelInfo excelInfo, int rowNum
     */
    private void createSpeakerExcelInfoRow(Workbook workbook, Sheet sheet, ExcelInfo excelInfo, int rowNum)
    {
        Row row = sheet.createRow(rowNum);
        CellStyle rowStyle = workbook.createCellStyle();
        Cell cell = null;

        // set filename:
        if (excelInfo.getFileName() != null)
        {
            cell = row.createCell(0);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getFileName());
        }

        // set subject Id:
        if (excelInfo.getSubjectId() != -1)
        {
            cell = row.createCell(1);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(String.format("%03d", excelInfo.getSubjectId()));
        }

        // skip country: column 2
        // set Company:
        if (excelInfo.getCompany() != null)
        {
            cell = row.createCell(3);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getCompany().toUpperCase());
        }

        // skip device, column 4
        // set session:
        if (excelInfo.getSessionId() != -1)
        {
            cell = row.createCell(5);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getSubjectId());
        }

        // set prompt:
        if (excelInfo.getPrompt() != null)
        {
            cell = row.createCell(6);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getPrompt());
        }

        // set domain
        if (excelInfo.getDomain() != null)
        {
            cell = row.createCell(7);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getDomain());
        }

        // set position:
        if (excelInfo.getPosition() != null)
        {
            cell = row.createCell(8);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getPosition());
        }

        // set bargeIn
        if (excelInfo.getBargeIn() != null)
        {
            cell = row.createCell(9);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getBargeIn());
        }

        // set relevancy grading
        if (excelInfo.getRelevancyGrading() != null)
        {
            cell = row.createCell(10);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getRelevancyGrading());
        }

        // set recording date:
        if (excelInfo.getRecodingDate() != null)
        {
            cell = row.createCell(11);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getRecordDateInFormat());
        }

        // set invocation attempts:
        cell = row.createCell(12);
        cell.setCellStyle(rowStyle);
        cell.setCellValue(excelInfo.getInvocationAttempts());

        // set LightOn
        cell = row.createCell(13);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getLightOn() != -1)
        {
            cell.setCellValue(excelInfo.getLightOn());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set user start:
        cell = row.createCell(14);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getUserStart() != -1)
        {
            cell.setCellValue(excelInfo.getUserStart());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set user end:
        cell = row.createCell(15);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getUserEnd() != -1)
        {
            cell.setCellValue(excelInfo.getUserEnd());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set reference:
        if (excelInfo.getReference() != null)
        {
            cell = row.createCell(16);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getReference());
        }

        // set hypothesis:
        if (excelInfo.getHypothesis() != null)
        {
            cell = row.createCell(17);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getHypothesis());
        }

        // set Hypothesis WER: column 18
        if (excelInfo.getHypothesisWER() != -1)
        {
            cell = row.createCell(18);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getHypothesisWER());
        }
        // skip SpeechError: column 19

        // set MusicDuckingOn:
        cell = row.createCell(20);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getMusicDuckingOn() != -1)
        {
            cell.setCellValue(excelInfo.getMusicDuckingOn());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set MusicDuckingOff
        cell = row.createCell(21);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getMusicDuckingOff() != -1)
        {
            cell.setCellValue(excelInfo.getMusicDuckingOff());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set LightOff
        cell = row.createCell(22);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getLightOff() != -1)
        {
            cell.setCellValue(excelInfo.getLightOff());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set System Response:
        if (excelInfo.getSystemResponse() != null)
        {
            cell = row.createCell(23);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getSystemResponse());
        }

        // set Category:
        if (excelInfo.getCategory() != null)
        {
            cell = row.createCell(24);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getCategory());
        }

        // set superBadResult:
        cell = row.createCell(25);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getSuperBadResult() != -1)
        {
            cell.setCellValue(excelInfo.getSuperBadResult());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set Response Audio
        cell = row.createCell(26);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getResponseAudio() != null)
        {
            cell.setCellValue(excelInfo.getResponseAudio());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set result:
        if (excelInfo.getResult() != null)
        {
            cell = row.createCell(27);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getResult());
        }

        // skip other comment: column 28
        // set TTSStart:
        cell = row.createCell(29);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getTtsStart() != -1)
        {
            cell.setCellValue(excelInfo.getTtsStart());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set TTSEnd
        cell = row.createCell(30);
        cell.setCellStyle(rowStyle);
        if (excelInfo.getTtsEnd() != -1)
        {
            cell.setCellValue(excelInfo.getTtsEnd());
        }
        else
        {
            cell.setCellValue(NA);
        }

        // set coder:
        if (excelInfo.getCoder() != null)
        {
            cell = row.createCell(31);
            cell.setCellStyle(rowStyle);
            cell.setCellValue(excelInfo.getCoder());
        }
    }

    private void createHeaderRow(Workbook workbook, Sheet sheet, String type)
    {
        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short) 11);
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create Header Row
        Row headerRow = sheet.createRow(0);
        List<String> headers = null;
        if (StringUtils.equalsIgnoreCase(StringHelper.PHONE, type))
        {
            headers = ExcelHeader.phoneHeaders;
        }
        else
        {
            headers = ExcelHeader.speakerHeaders;
        }

        for (int i = 0, n = headers.size(); i < n; i++)
        {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(headers.get(i));
            cell.setCellStyle(headerCellStyle);
        }
    }

}
