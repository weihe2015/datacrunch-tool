package org.gs4tr.dataforce.datacrunch.tool.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.json.JSONObject;

@Entity
@Table(name = "hypothesis")
public class Hypothesis implements Serializable
{
    private static final long serialVersionUID = 2100656269840998465L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "company", nullable = false)
    private String company;

    @Column(name = "hypothesis", nullable = false)
    private String hypothesis;

    @Column(name = "date_month", nullable = true)
    private String month;

    @Column(name = "date_day", nullable = true)
    private int day;

    @Column(name = "date_hour", nullable = true)
    private int hour;

    @Column(name = "date_minute", nullable = true)
    private int minute;

    @Column(name = "date_second", nullable = true)
    private int second;

    public Hypothesis()
    {

    }

    public String getCompany()
    {
        return company;
    }

    public void setCompany(String company)
    {
        this.company = company;
    }

    public String getHypothesis()
    {
        return hypothesis;
    }

    public void setHypothesis(String hypothesis)
    {
        this.hypothesis = hypothesis;
    }

    public String getMonth()
    {
        return month;
    }

    public void setMonth(String month)
    {
        this.month = month;
    }

    public int getDay()
    {
        return day;
    }

    public void setDay(int day)
    {
        this.day = day;
    }

    public int getHour()
    {
        return hour;
    }

    public void setHour(int hour)
    {
        this.hour = hour;
    }

    public int getMinute()
    {
        return minute;
    }

    public void setMinute(int minute)
    {
        this.minute = minute;
    }

    public int getSecond()
    {
        return second;
    }

    public void setSecond(int second)
    {
        this.second = second;
    }

    @Override
    public String toString()
    {
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("id", this.id);
        jsonObj.put("company", this.company);
        jsonObj.put("hypothesis", this.hypothesis);
        jsonObj.put("month", this.month);
        jsonObj.put("day", this.day);
        jsonObj.put("hour", this.hour);
        jsonObj.put("minute", this.minute);
        jsonObj.put("second", this.second);
        return jsonObj.toString(2);
    }

}
