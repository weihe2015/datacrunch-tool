package org.gs4tr.dataforce.datacrunch.tool.service.text.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.gs4tr.dataforce.datacrunch.tool.Messages;
import org.gs4tr.dataforce.datacrunch.tool.service.text.TextFileReaderService;
import org.gs4tr.foundation3.encoding.CharsetPrinter;

public class TextFileReaderServiceImpl implements TextFileReaderService
{
    @Override
    public List<String> getContents(String fileName)
    {
        List<String> contents = new ArrayList<>();
        Reader reader = null;
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        try
        {
            String encoding = new CharsetPrinter().guessEncoding(new File(fileName));
            if (StringUtils.equalsIgnoreCase("void", encoding))
            {
                encoding = StandardCharsets.UTF_8.name();
            }
            fileInputStream = new FileInputStream(fileName);
            inputStreamReader = new InputStreamReader(fileInputStream, encoding);
            reader = new LineNumberReader(inputStreamReader);

            String line;
            LineNumberReader lineNumberReader = (LineNumberReader) reader;
            while ((line = lineNumberReader.readLine()) != null)
            {
                line = line.trim();
                contents.add(line);
            }
        }
        catch (Throwable ex)
        {
            LOG.error(String.format(Messages.getString("TextFileReader.2"), fileName, ex.getMessage()));
        }
        finally
        {
            IOUtils.closeQuietly(inputStreamReader);
            IOUtils.closeQuietly(fileInputStream);
            IOUtils.closeQuietly(reader);
        }
        return contents;
    }
}
