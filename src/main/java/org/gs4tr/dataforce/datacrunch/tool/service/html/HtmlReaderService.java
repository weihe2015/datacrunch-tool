package org.gs4tr.dataforce.datacrunch.tool.service.html;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("HtmlReaderService")
public interface HtmlReaderService
{
    static public Logger LOG = LoggerFactory.getLogger(HtmlReaderService.class);
    static public final String CONTENT_CELL = "content-cell";

    public Document getDocument(String fileName);

    public Elements getContentDivElements(Document doc);
}
