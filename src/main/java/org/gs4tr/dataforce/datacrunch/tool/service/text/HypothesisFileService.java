package org.gs4tr.dataforce.datacrunch.tool.service.text;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("HypothesisFileService")
public interface HypothesisFileService
{
    static public Logger LOG = LoggerFactory.getLogger(HypothesisFileService.class);
    static public final String AMAZON = "Amazon";
    static public final String APPLE = "Apple";
    static public final String GOOGLE = "Google";
    static public final String CONTENT_PREFIX = "Said ";
    static public final String CSV_SPLITTED_WITH_QUOTE = ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
    static public final String DATE_FORMAT_PARSE_REGEX = "MMM d, yyyy, HH:mm:ss a zzz";
    static public final Pattern CONTENT_PATTERN = Pattern.compile(
        "Said(.*)((Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\\s[\\d]{1,2},\\s[\\d]{4},\\s[\\d]{1,2}:[\\d]{1,2}:[\\d]{1,2}\\s(AM|PM)\\s[a-zA-Z]{3,5})",
        Pattern.CASE_INSENSITIVE);

    public void processHypothesisFiles(List<String> hypothesisFiles, Map<String, Set<String>> hypothesisMapping);

    public void processHypothesisFile(String hypothesisFile, Map<String, Set<String>> hypothesisMapping);

    public void processGoogleHtmlFile(String hypothesisFile, Map<String, Set<String>> hypothesisMapping);
}
