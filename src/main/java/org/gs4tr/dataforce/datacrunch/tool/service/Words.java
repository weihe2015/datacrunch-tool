package org.gs4tr.dataforce.datacrunch.tool.service;

public class Words
{
    public static int minDistance(String word1, String word2)
    {
        // Ignore case difference:
        word1 = word1.toLowerCase();
        word2 = word2.toLowerCase();
        int m = word1.length(), n = word2.length();
        int[][] dp = new int[m + 1][n + 1];
        for (int i = 0; i <= m; i++)
        {
            dp[i][0] = i;
        }
        for (int j = 0; j <= n; j++)
        {
            dp[0][j] = j;
        }
        for (int i = 1; i <= m; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                if (word1.charAt(i - 1) == word2.charAt(j - 1))
                {
                    dp[i][j] = dp[i - 1][j - 1];
                }
                else
                {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                }
                int minVal = Math.min(dp[i - 1][j] + 1, dp[i][j - 1] + 1);
                dp[i][j] = Math.min(dp[i][j], minVal);
            }
        }
        return dp[m][n];
    }
}
