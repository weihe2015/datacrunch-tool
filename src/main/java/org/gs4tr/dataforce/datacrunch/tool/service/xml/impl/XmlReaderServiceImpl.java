package org.gs4tr.dataforce.datacrunch.tool.service.xml.impl;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Node;
import org.gs4tr.dataforce.datacrunch.tool.Messages;
import org.gs4tr.dataforce.datacrunch.tool.service.xml.XmlReaderService;
import org.gs4tr.foundation3.encoding.CharsetPrinter;
import org.gs4tr.foundation3.xml.XmlParser;

public class XmlReaderServiceImpl implements XmlReaderService
{
    @Override
    public Document getDocument(String fileName)
    {
        Document doc = null;

        try
        {
            String encoding = new CharsetPrinter().guessEncoding(new File(fileName));
            doc = XmlParser.parseXmlFile(fileName, encoding);
        }
        catch (Throwable ex)
        {
            fileName = FilenameUtils.getName(fileName);
            LOG.error(String.format(Messages.getString("AnvilService.2"), fileName, ex.getMessage()));
        }
        return doc;
    }

    @Override
    public Element getRootElement(Document doc)
    {
        return doc.getRootElement();
    }

    @Override
    public String getVideoFilename(Element root)
    {
        Node node = root.selectSingleNode("//*[name()='video']/@src");
        if (node == null)
        {
            return "";
        }
        return node.getText();
    }

    @Override
    public String getCoderName(Element root)
    {
        Node node = root.selectSingleNode("//*[name()='info' and (@key='coder')]");
        if (node == null)
        {
            return "";
        }
        return node.getText();
    }

    @Override
    public double getButtonPress(Element root)
    {
        String xpath = "//track[@name='Timestamps.ButtonPress/Action']/el/@start";
        Attribute attribute = (Attribute) root.selectSingleNode(xpath);
        if (attribute == null)
        {
            return -1;
        }
        String buttonPressText = attribute.getValue();
        double buttonPress = -1;
        try
        {
            buttonPress = Double.parseDouble(buttonPressText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.9"), "button press", ex.getMessage()));
        }
        return buttonPress;
    }

    @Override
    public double getActionStart(Element root)
    {
        String xpath = "//track[@name='Timestamps.ButtonPress/Action']/el/@end";
        Attribute attribute = (Attribute) root.selectSingleNode(xpath);
        if (attribute == null)
        {
            return -1;
        }
        String actionStartText = attribute.getValue();
        double actionStart = -1;
        try
        {
            actionStart = Double.parseDouble(actionStartText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.9"), "action start", ex.getMessage()));
        }
        return actionStart;
    }

    @Override
    public double getUserStart(Element root)
    {
        String xpath = "//track[@name='Transcription.Reference']/el/@start";
        Attribute attribute = (Attribute) root.selectSingleNode(xpath);
        if (attribute == null)
        {
            return -1;
        }
        String userStartText = attribute.getValue();
        double userStart = -1;
        try
        {
            userStart = Double.parseDouble(userStartText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.9"), "user start", ex.getMessage()));
        }
        return userStart;
    }

    @Override
    public double getUserEnd(Element root)
    {
        String xpath = "//track[@name='Transcription.Reference']/el/@end";
        Attribute attribute = (Attribute) root.selectSingleNode(xpath);
        if (attribute == null)
        {
            return -1;
        }
        String userEndText = attribute.getValue();
        double userEnd = -1;
        try
        {
            userEnd = Double.parseDouble(userEndText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.9"), "user end", ex.getMessage()));
        }
        return userEnd;
    }

    @Override
    public String getReference(Element root)
    {
        Node node = root.selectSingleNode("//attribute[@name='Reference']");
        if (node == null)
        {
            return "";
        }
        return node.getText();
    }

    @Override
    public String getHypothesis(Element root)
    {
        Node node = root.selectSingleNode("//attribute[@name='Hypothesis']");
        if (node == null)
        {
            return "";
        }
        return node.getText();
    }

    @Override
    public double getTextDisplayStarts(Element root)
    {
        String xpath = "//track[@name='Transcription.Hypothesis']/el/@start";
        Attribute attribute = (Attribute) root.selectSingleNode(xpath);
        if (attribute == null)
        {
            return -1;
        }
        String textDisplayStartsText = attribute.getValue();
        double textDisplayStarts = -1;
        try
        {
            textDisplayStarts = Double.parseDouble(textDisplayStartsText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.9"), "text display starts", ex.getMessage()));
        }
        return textDisplayStarts;
    }

    @Override
    public double getTextDisplayEnds(Element root)
    {
        String xpath = "//track[@name='Transcription.Hypothesis']/el/@end";
        Attribute attribute = (Attribute) root.selectSingleNode(xpath);
        if (attribute == null)
        {
            return -1;
        }
        String textDisplayEndsText = attribute.getValue();
        double textDisplayEnds = -1;
        try
        {
            textDisplayEnds = Double.parseDouble(textDisplayEndsText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.9"), "text display ends", ex.getMessage()));
        }
        return textDisplayEnds;
    }

    @Override
    public String getSystemResponse(Element root)
    {
        Node node = root.selectSingleNode("//attribute[@name='E2E']");
        if (node == null)
        {
            return "";
        }
        return node.getText();
    }

    @Override
    public String getCategory(Element root)
    {
        Node node = root.selectSingleNode("//attribute[@name='Category']");
        if (node == null)
        {
            return "";
        }
        return node.getText();
    }

    @Override
    public int getSuperBadResult(Element root)
    {
        Node node = root.selectSingleNode("//attribute[@name='SuperBadResult']");
        if (node == null)
        {
            return -1;
        }
        String superBadResultText = node.getText();
        int superBadResult = -1;
        try
        {
            superBadResult = Integer.parseInt(superBadResultText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.10"), "super bad result", ex.getMessage()));
        }
        return superBadResult;
    }

    @Override
    public String getResponseAudio(Element root)
    {
        Node node = root.selectSingleNode("//attribute[@name='ResponseAudio']");
        if (node == null)
        {
            return "";
        }
        return node.getText();
    }

    @Override
    public String getResult(Element root)
    {
        Node node = root.selectSingleNode("//attribute[@name='Result']");
        if (node == null)
        {
            return "";
        }
        return node.getText();
    }

    @Override
    public double getTTSStart(Element root)
    {
        String xpath = "//track[@name='Transcription.Response']/el/@start";
        Attribute attribute = (Attribute) root.selectSingleNode(xpath);
        if (attribute == null)
        {
            return -1;
        }
        String ttsStartText = attribute.getValue();
        double ttsStart = -1;
        try
        {
            ttsStart = Double.parseDouble(ttsStartText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.9"), "ttsStart", ex.getMessage()));
        }
        return ttsStart;
    }

    @Override
    public double getTTSEnd(Element root)
    {
        String xpath = "//track[@name='Transcription.Response']/el/@end";
        Attribute attribute = (Attribute) root.selectSingleNode(xpath);
        if (attribute == null)
        {
            return -1;
        }
        String ttsEndText = attribute.getValue();
        double ttsEnd = -1;
        try
        {
            ttsEnd = Double.parseDouble(ttsEndText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.9"), "ttsEnd", ex.getMessage()));
        }
        return ttsEnd;
    }

    @Override
    public double getLightOn(Element root)
    {
        String xpath = "//track[@name='Timestamps.Light']/el/@start";
        Attribute attribute = (Attribute) root.selectSingleNode(xpath);
        if (attribute == null)
        {
            return -1;
        }
        String lightOnText = attribute.getValue();
        double lightOn = -1;
        try
        {
            lightOn = Double.parseDouble(lightOnText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.9"), "light On", ex.getMessage()));
        }
        return lightOn;
    }

    @Override
    public double getLightOff(Element root)
    {
        String xpath = "//track[@name='Timestamps.Light']/el/@end";
        Attribute attribute = (Attribute) root.selectSingleNode(xpath);
        if (attribute == null)
        {
            return -1;
        }
        String lightOffText = attribute.getValue();
        double lightOff = -1;
        try
        {
            lightOff = Double.parseDouble(lightOffText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.9"), "light Off", ex.getMessage()));
        }
        return lightOff;
    }

    @Override
    public int getInvocationAttempts(Element root)
    {
        Node node = root.selectSingleNode("//attribute[@name='InvocationAttempts']");
        if (node == null)
        {
            return -1;
        }
        String invocationAttemptsText = node.getText();
        int invocationAttempts = -1;
        try
        {
            invocationAttempts = Integer.parseInt(invocationAttemptsText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.10"), "invocation", ex.getMessage()));
        }
        return invocationAttempts;
    }

    @Override
    public double getMusicDuckingOn(Element root)
    {
        String xpath = "//track[@name='Transcription.AudioDucking']/el/@start";
        Attribute attribute = (Attribute) root.selectSingleNode(xpath);
        if (attribute == null)
        {
            return -1;
        }
        String musicDuckingOnText = attribute.getValue();
        double musicDuckingOn = -1;
        try
        {
            musicDuckingOn = Double.parseDouble(musicDuckingOnText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.9"), "music ducking on", ex.getMessage()));
        }
        return musicDuckingOn;
    }

    @Override
    public double getMusicDuckingOff(Element root)
    {
        String xpath = "//track[@name='Transcription.AudioDucking']/el/@end";
        Attribute attribute = (Attribute) root.selectSingleNode(xpath);
        if (attribute == null)
        {
            return -1;
        }
        String musicDuckingOffText = attribute.getValue();
        double musicDuckingOff = -1;
        try
        {
            musicDuckingOff = Double.parseDouble(musicDuckingOffText);
        }
        catch (NumberFormatException ex)
        {
            LOG.warn(String.format(Messages.getString("AnvilService.9"), "music ducking off", ex.getMessage()));
        }
        return musicDuckingOff;
    }

    @Override
    public String getResponseVisual(Element root)
    {
        Node node = root.selectSingleNode("//attribute[@name='ResponseVisual']");
        if (node == null)
        {
            return "";
        }
        return node.getText();
    }

    @Override
    public String getFormat(Element root)
    {
        Node node = root.selectSingleNode("//attribute[@name='Format']");
        if (node == null)
        {
            return "";
        }
        return node.getText();
    }

}
