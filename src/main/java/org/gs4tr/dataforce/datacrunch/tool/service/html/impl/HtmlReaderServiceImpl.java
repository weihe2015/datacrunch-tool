package org.gs4tr.dataforce.datacrunch.tool.service.html.impl;

import java.io.File;

import org.apache.commons.io.FilenameUtils;
import org.gs4tr.dataforce.datacrunch.tool.Messages;
import org.gs4tr.dataforce.datacrunch.tool.service.html.HtmlReaderService;
import org.gs4tr.foundation3.encoding.CharsetPrinter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class HtmlReaderServiceImpl implements HtmlReaderService
{
    @Override
    public Document getDocument(String fileName)
    {
        Document doc = null;
        try
        {
            String encoding = new CharsetPrinter().guessEncoding(new File(fileName));
            doc = Jsoup.parse(new File(fileName), encoding);
        }
        catch (Throwable ex)
        {
            fileName = FilenameUtils.getName(fileName);
            LOG.error(String.format(Messages.getString("HypothesisGoogleHtmlService.1"), fileName, ex.getMessage()));
        }
        return doc;
    }

    @Override
    public Elements getContentDivElements(Document doc)
    {
        if (doc == null)
        {
            return null;
        }
        return doc.getElementsByClass(CONTENT_CELL);
    }

}
