package org.gs4tr.dataforce.datacrunch.tool;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.dataforce.datacrunch.tool.model.AnvilReference;
import org.gs4tr.dataforce.datacrunch.tool.model.ExcelInfo;
import org.gs4tr.dataforce.datacrunch.tool.model.Hypothesis;
import org.gs4tr.dataforce.datacrunch.tool.model.Prompt;
import org.gs4tr.dataforce.datacrunch.tool.model.Satisfaction;
import org.gs4tr.dataforce.datacrunch.tool.repository.AnvilReferenceRepository;
import org.gs4tr.dataforce.datacrunch.tool.repository.HypothesisRepository;
import org.gs4tr.dataforce.datacrunch.tool.repository.PromptRepository;
import org.gs4tr.dataforce.datacrunch.tool.repository.SatisfactionRepository;
import org.gs4tr.dataforce.datacrunch.tool.service.Words;
import org.gs4tr.dataforce.datacrunch.tool.service.excel.ExcelWriterService;
import org.gs4tr.dataforce.datacrunch.tool.service.file.FileService;
import org.gs4tr.dataforce.datacrunch.tool.service.string.StringHelper;
import org.gs4tr.dataforce.datacrunch.tool.service.text.HypothesisFileService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.PromptFileService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.SatisfactionFileService;
import org.gs4tr.dataforce.datacrunch.tool.service.xml.AnvilService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatacrunchToolApplication implements CommandLineRunner
{
    private static Logger LOG = LoggerFactory.getLogger(DatacrunchToolApplication.class);

    public static void main(String[] args)
    {
        LOG.info("STARTING THE APPLICATION");
        SpringApplication.run(DatacrunchToolApplication.class, args);
        LOG.info("APPLICATION FINISHED");
    }

    @Autowired
    private FileService fileService;

    @Autowired
    private AnvilService anvilService;

    @Autowired
    private PromptFileService promptFileService;

    @Autowired
    private SatisfactionFileService satisfactionFileService;

    @Autowired
    private HypothesisFileService hypothesisFileService;

    @Autowired
    private ExcelWriterService excelWriterService;

    @Autowired
    private AnvilReferenceRepository anvilReferenceRepository;

    @Autowired
    private HypothesisRepository hypothesisRepository;

    @Autowired
    private PromptRepository promptRepository;

    @Autowired
    private SatisfactionRepository satisfactionRepository;

    private List<AnvilReference> anvilReferenceList;
    private List<Hypothesis> hypothesisList;
    private List<Prompt> promptList;
    private List<Satisfaction> satisfactionList;

    @Override
    public void run(String... args) throws Exception
    {
        if (args.length != 4)
        {
            LOG.error(Messages.getString("MainService.1"));
            return;
        }
        String inputFolderPath = args[1];
        String outputFolderPath = args[2];
        String type = args[3].trim();

        if (!new File(inputFolderPath).exists())
        {
            LOG.error(Messages.getString("MainService.2"));
            return;
        }

        if (!StringUtils.equalsIgnoreCase(StringHelper.PHONE, type) &&
            !StringUtils.equalsIgnoreCase(StringHelper.SPEAKER, type))
        {
            LOG.error(Messages.getString("MainService.3"));
            return;
        }

        File outputFolder = new File(outputFolderPath);
        if (!outputFolder.exists())
        {
            outputFolder.mkdirs();
        }

        LOG.info(Messages.getString("MainService.4"));
        List<String> anvilFiles = new ArrayList<>();
        List<String> hypothesisFiles = new ArrayList<>();
        List<String> promptFiles = new ArrayList<>();
        List<String> satisfactionFiles = new ArrayList<>();

        fileService.getFiles(inputFolderPath, anvilFiles, hypothesisFiles, promptFiles, satisfactionFiles);

        readFiles(anvilFiles, hypothesisFiles, promptFiles, satisfactionFiles);

        anvilReferenceList = anvilReferenceRepository.findAll();
        hypothesisList = hypothesisRepository.findAll();
        promptList = promptRepository.findAll();
        satisfactionList = satisfactionRepository.findAll();

        int numOfAnvilReference = anvilReferenceList.size();
        int numOfHypothesis = hypothesisList.size();
        int numOfPrompt = promptList.size();
        int numOfSatisfaction = satisfactionList.size();

        LOG.info(String.format(Messages.getString("MainService.12"), numOfAnvilReference, numOfHypothesis, numOfPrompt,
            numOfSatisfaction));
        if (anvilReferenceList == null || anvilReferenceList.isEmpty())
        {
            LOG.warn(Messages.getString("MainService.5"));
            return;
        }
        if (StringUtils.equalsIgnoreCase(StringHelper.SPEAKER, type) &&
            (hypothesisList == null || hypothesisList.isEmpty()))
        {
            LOG.error(Messages.getString("MainService.11"));
            return;
        }
        if (promptList == null || promptList.isEmpty())
        {
            LOG.warn(Messages.getString("MainService.13"));
            return;
        }
        if (satisfactionList == null || satisfactionList.isEmpty())
        {
            LOG.warn(Messages.getString("MainService.14"));
            return;
        }

        processInfos(type, outputFolderPath);
    }

    private void readFiles(List<String> anvilFiles, List<String> hypothesisFiles, List<String> promptFiles,
        List<String> satisfactionFiles)
    {
        // Read all anvilFiles
        anvilService.processAnvilFiles(anvilFiles);

        // Read all prompt Files:
        promptFileService.processPromptFiles(promptFiles);

        // Read all satisfaction files:
        satisfactionFileService.processSatisfactionFiles(satisfactionFiles);

        // read all hypothesis Files:
        Map<String, Set<String>> hypothesisMapping = initializeHypothesisMapping();
        hypothesisFileService.processHypothesisFiles(hypothesisFiles, hypothesisMapping);
    }

    private Map<String, Set<String>> initializeHypothesisMapping()
    {
        Map<String, Set<String>> hypothesisMapping = new LinkedHashMap<>();
        hypothesisMapping.put(HypothesisFileService.AMAZON, new HashSet<>());
        hypothesisMapping.put(HypothesisFileService.APPLE, new HashSet<>());
        hypothesisMapping.put(HypothesisFileService.GOOGLE, new HashSet<>());
        return hypothesisMapping;
    }

    private void processInfos(String type, String outputFolderPath)
    {
        if (anvilReferenceList == null || anvilReferenceList.isEmpty())
        {
            LOG.warn(Messages.getString("MainService.5"));
            return;
        }
        List<ExcelInfo> excelInfos = new ArrayList<>();
        for (int i = 0, max = anvilReferenceList.size(); i < max; i++)
        {
            AnvilReference anvilReference = anvilReferenceList.get(i);
            int idx = i + 1;
            LOG.info(String.format(Messages.getString("MainService.6"), idx, max, anvilReference.getVideoFilename()));
            gatherAnvilWithOtherInfos(excelInfos, anvilReference, type);
        }

        // Begin to write infos to excel file
        LOG.info(Messages.getString("MainService.9"));
        String outputFileName = generateExcelOutputFileName(outputFolderPath);

        try
        {
            excelWriterService.write(outputFileName, excelInfos, type);
        }
        catch (IOException ex)
        {
            LOG.error(String.format(Messages.getString("MainService.10"), ex.getMessage()));
        }
    }

    private void gatherAnvilWithOtherInfos(List<ExcelInfo> excelInfos, AnvilReference anvilReference, String type)
    {
        ExcelInfo excelInfo = new ExcelInfo();
        excelInfo.saveInfoFromAnvilReference(anvilReference);

        int subjectId = anvilReference.getSubjectId();
        int promptId = anvilReference.getPromptId();
        int sessionId = anvilReference.getSessionId();

        // find prompt based on subjectId and promptId:
        Optional<Prompt> optionalPrompt = promptRepository.findBySubjectIdAndPromptId(subjectId, promptId);
        if (optionalPrompt.isEmpty())
        {
            LOG.warn(String.format(Messages.getString("MainService.7"), subjectId, promptId));
            return;
        }
        Prompt prompt = optionalPrompt.get();
        excelInfo.saveInfoFromPrompt(prompt);

        // find satisfaction based on subjectId, sessionId and promptId:
        try
        {
            Optional<Satisfaction> optionalSatisfaction = satisfactionRepository.findBySessionIdAndSubjectIdAndPromptId(
                sessionId, subjectId, promptId);
            if (optionalSatisfaction.isEmpty())
            {
                LOG.warn(String.format(Messages.getString("MainService.8"), sessionId, subjectId, promptId));
                return;
            }
            Satisfaction satisfaction = optionalSatisfaction.get();
            excelInfo.saveInfoFromSatisfaction(satisfaction);
        }
        catch (Exception ex)
        {
            LOG.error(
                String.format(Messages.getString("MainService.15"), sessionId, subjectId, promptId, ex.getMessage()));
        }

        // If type is speaker, we need to find all hypothesis we saved in the database,
        // calculate the edit distance and find the most matches one:
        if (StringUtils.equalsIgnoreCase(StringHelper.SPEAKER, type))
        {
            String company = anvilReference.getCompany().toLowerCase();
            List<Hypothesis> hypothesisWithCompanyList = hypothesisRepository.findByCompany(company);
            findMostRelevantHypothesis(hypothesisWithCompanyList, anvilReference, excelInfo);
        }
        excelInfos.add(excelInfo);
    }

    private void findMostRelevantHypothesis(List<Hypothesis> hypothesisWithCompanyList, AnvilReference anvilReference,
        ExcelInfo excelInfo)
    {
        String referenceText = anvilReference.getReference();
        Queue<ReferenceHypothesis> pq = new PriorityQueue<ReferenceHypothesis>(new Comparator<ReferenceHypothesis>()
        {
            @Override
            public int compare(ReferenceHypothesis rh1, ReferenceHypothesis rh2)
            {
                // ReferenceHypothesis object with lower errorRate will be ranked higher.
                if (rh1.errorRate < rh2.errorRate)
                {
                    return -1;
                }
                else if (rh1.errorRate == rh2.errorRate)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
        });

        for (Hypothesis hypothesis : hypothesisWithCompanyList)
        {
            ReferenceHypothesis rh = new ReferenceHypothesis();
            String hypothesisText = hypothesis.getHypothesis();
            rh.reference = referenceText;
            rh.hypothesis = hypothesisText;

            int editDistance = Words.minDistance(referenceText, hypothesisText);
            // set both Numerator and Denominator with double type, otherwise it will not have decimal
            double errorRate = (double) editDistance / (double) referenceText.length();
            rh.editDistance = editDistance;
            rh.errorRate = errorRate;
            pq.offer(rh);
        }
        if (!pq.isEmpty())
        {
            ReferenceHypothesis rhCandidate = pq.peek();
            excelInfo.setHypothesis(rhCandidate.hypothesis);
            excelInfo.setHypothesisWER(rhCandidate.editDistance);
        }
    }

    private String generateExcelOutputFileName(String outputFolderPath)
    {
        StringBuffer sb = new StringBuffer();
        String date = new SimpleDateFormat(StringHelper.DATE_SPECIFIC_FORMAT2).format(new Date());
        sb.append(outputFolderPath);
        sb.append(File.separator);
        sb.append(date);
        sb.append(FileService.XLSX_EXTENSION);
        return sb.toString();
    }

    class ReferenceHypothesis
    {
        String reference;
        String hypothesis;
        int editDistance;
        double errorRate;

        public ReferenceHypothesis()
        {

        }
    }
}
