package org.gs4tr.dataforce.datacrunch.tool.service.text.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.gs4tr.dataforce.datacrunch.tool.Messages;
import org.gs4tr.dataforce.datacrunch.tool.model.Prompt;
import org.gs4tr.dataforce.datacrunch.tool.repository.PromptRepository;
import org.gs4tr.dataforce.datacrunch.tool.service.file.FilenameService;
import org.gs4tr.dataforce.datacrunch.tool.service.string.StringHelper;
import org.gs4tr.dataforce.datacrunch.tool.service.text.HypothesisFileService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.PromptFileService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.TextFileReaderService;
import org.springframework.beans.factory.annotation.Autowired;

public class PromptFileServiceImpl implements PromptFileService
{
    @Autowired
    private TextFileReaderService textFileReaderService;

    @Autowired
    private PromptRepository promptRepository;

    @Autowired
    private FilenameService filenameService;

    @Override
    public void processPromptFiles(List<String> promptFiles)
    {
        if (promptFiles == null || promptFiles.isEmpty())
        {
            LOG.warn(Messages.getString("PromptService.1"));
            return;
        }
        for (String promptFile : promptFiles)
        {
            processPromptFile(promptFile);
        }
    }

    @Override
    public void processPromptFile(String promptFile)
    {
        String filename = FilenameUtils.getName(promptFile);
        LOG.info(String.format(Messages.getString("PromptService.2"), filename));

        // get subjectId from prompt filename
        Map<String, String> infoMap = filenameService.getInfoMapFromPromptFileName(promptFile);
        int subjectId = -1;
        if (infoMap.containsKey(FilenameService.SUBJECT_ID))
        {
            String subjectIdText = infoMap.get(FilenameService.SUBJECT_ID);
            subjectId = Integer.parseInt(subjectIdText);
        }
        else
        {
            LOG.warn(String.format(Messages.getString("PromptService.4"), filename));
        }

        List<String> contents = textFileReaderService.getContents(promptFile);
        for (int i = 0, max = contents.size(); i < max; i++)
        {
            String content = contents.get(i);
            int promptId = i + 1;
            processPromptContent(promptId, filename, content, subjectId);
        }
    }

    private void processPromptContent(int promptId, String promptFile, String content, int subjectId)
    {
        if (StringUtils.isBlank(content))
        {
            return;
        }
        Prompt prompt = new Prompt();
        prompt.setSubjectId(subjectId);

        // split the content by comma:
        String[] splitted = content.split(HypothesisFileService.CSV_SPLITTED_WITH_QUOTE);

        if (splitted.length != 3 && splitted.length != 4)
        {
            LOG.warn(String.format(Messages.getString("PromptService.3"), promptFile, content));
            return;
        }

        prompt.setPromptId(promptId);
        prompt.setPrompt(StringHelper.removeQuote(splitted[0]));
        prompt.setDomain(splitted[1]);
        prompt.setPosition(splitted[2]);
        if (splitted.length == 4)
        {
            prompt.setBargeIn(splitted[3]);
        }
        // save prompt to database:
        promptRepository.save(prompt);
    }
}
