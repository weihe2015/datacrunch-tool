package org.gs4tr.dataforce.datacrunch.tool.hypothesis;

import java.util.Map;

import org.gs4tr.dataforce.datacrunch.tool.service.file.FilenameService;
import org.gs4tr.dataforce.datacrunch.tool.service.file.impl.FilenameServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class HypothesisFileNameTest
{
    @TestConfiguration
    static class FilenameServiceContextConfiguration
    {
        @Bean
        public FilenameService filenameService()
        {
            return new FilenameServiceImpl();
        }
    }

    @Autowired
    private FilenameService filenameService;

    @Test
    public void hypothesisFileNameTest()
    {
        String hypothesisFileName = "target/test-classes/Amazon_US_Speaker_2019-06-10_to_2019-06-24.csv";
        Map<String, String> infoMap = filenameService.getInfoMapFromHypothesisFileName(hypothesisFileName);
        Assert.assertFalse(infoMap.isEmpty());
        Assert.assertEquals("Amazon", infoMap.get(FilenameServiceImpl.COMPANY));

        hypothesisFileName = "target/test-classes/Apple_US_Speaker_2019-06-18_to_2019-06-26.csv";
        infoMap = filenameService.getInfoMapFromHypothesisFileName(hypothesisFileName);
        Assert.assertFalse(infoMap.isEmpty());
        Assert.assertEquals("Apple", infoMap.get(FilenameServiceImpl.COMPANY));

        hypothesisFileName = "target/test-classes/Google001.csv";
        infoMap = filenameService.getInfoMapFromHypothesisFileName(hypothesisFileName);
        Assert.assertFalse(infoMap.isEmpty());
        Assert.assertEquals("Google", infoMap.get(FilenameServiceImpl.COMPANY));
    }
}
