package org.gs4tr.dataforce.datacrunch.tool.anvil;

import java.io.File;

import org.dom4j.Document;
import org.dom4j.Element;
import org.gs4tr.dataforce.datacrunch.tool.service.xml.XmlReaderService;
import org.gs4tr.dataforce.datacrunch.tool.service.xml.impl.XmlReaderServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class AnvilFileReaderTest
{
    @TestConfiguration
    static class AnvilFileReaderContextConfiguration
    {
        @Bean
        public XmlReaderService xmlReaderService()
        {
            return new XmlReaderServiceImpl();
        }
    }

    @Autowired
    private XmlReaderService xmlReaderService;

    @Test
    public void anvilReferenceFileReaderTest1() throws Exception
    {
        String testFilePath = "target/test-classes/s007_Amazon_session2_pr001_20190710.anvil";
        File testFile = new File(testFilePath);
        Assert.assertTrue(testFile.exists());
        Document doc = xmlReaderService.getDocument(testFilePath);
        Assert.assertNotNull(doc);

        Element root = xmlReaderService.getRootElement(doc);
        Assert.assertEquals("../Subject007/s007_Amazon_session2_pr001_20190710.avi",
            xmlReaderService.getVideoFilename(root));

        Assert.assertEquals("Erin", xmlReaderService.getCoderName(root));
        Assert.assertEquals(2.936, xmlReaderService.getButtonPress(root), 0.0001);
        Assert.assertEquals(15.748, xmlReaderService.getActionStart(root), 0.0001);
        Assert.assertEquals(1.334, xmlReaderService.getUserStart(root), 0.0001);
        Assert.assertEquals(4.904, xmlReaderService.getUserEnd(root), 0.0001);
        Assert.assertEquals("Alexa make the bathroom light brighter", xmlReaderService.getReference(root));
        Assert.assertNull(xmlReaderService.getHypothesis(root));
        Assert.assertEquals(-1, xmlReaderService.getTextDisplayEnds(root), 0.0001);
        Assert.assertEquals("Failure", xmlReaderService.getSystemResponse(root));
        Assert.assertEquals("C.1.3.5 Action, None (3)", xmlReaderService.getCategory(root));
        Assert.assertEquals(0, xmlReaderService.getSuperBadResult(root));
        Assert.assertEquals("Bathroom light doesn't support that", xmlReaderService.getResponseAudio(root));
        Assert.assertEquals("Unsuccessful. User says make bathroom light brighter, " +
            "device says bathroom light doesn't support that",
            xmlReaderService.getResult(root));
        Assert.assertEquals(15.782, xmlReaderService.getTTSStart(root), 0.0001);
        Assert.assertEquals(17.817, xmlReaderService.getTTSEnd(root), 0.0001);
    }

    @Test
    public void anvilReferenceFileReaderTest2() throws Exception
    {
        String testFilePath = "target/test-classes/s007_Amazon_session2_pr009_20190710.anvil";
        File testFile = new File(testFilePath);
        Assert.assertTrue(testFile.exists());
        Document doc = xmlReaderService.getDocument(testFilePath);
        Assert.assertNotNull(doc);

        Element root = xmlReaderService.getRootElement(doc);
        Assert.assertEquals("../Subject007/s007_Amazon_session2_pr009_20190710.avi",
            xmlReaderService.getVideoFilename(root));

        Assert.assertEquals("Erin", xmlReaderService.getCoderName(root));
        Assert.assertEquals(13.279, xmlReaderService.getButtonPress(root), 0.0001);
        Assert.assertEquals(21.12, xmlReaderService.getActionStart(root), 0.0001);
        Assert.assertEquals(11.978, xmlReaderService.getUserStart(root), 0.0001);
        Assert.assertEquals(16.749, xmlReaderService.getUserEnd(root), 0.0001);
        Assert.assertEquals("Alexa play Déjà Vu Affair by Sofi Tukker", xmlReaderService.getReference(root));
        Assert.assertEquals(26.125, xmlReaderService.getTextDisplayEnds(root), 0.0001);
        Assert.assertEquals("Success", xmlReaderService.getSystemResponse(root));
        Assert.assertEquals("n/a", xmlReaderService.getCategory(root));
        Assert.assertEquals(0, xmlReaderService.getSuperBadResult(root));
        Assert.assertEquals("Here's Déjà Vu Affair by Sofi Tukker on Amazon Music",
            xmlReaderService.getResponseAudio(root));
        Assert.assertEquals("Success. User says play song, device plays correct song",
            xmlReaderService.getResult(root));
        Assert.assertEquals(21.42, xmlReaderService.getTTSStart(root), 0.0001);
        Assert.assertEquals(25.725, xmlReaderService.getTTSEnd(root), 0.0001);
    }

}
