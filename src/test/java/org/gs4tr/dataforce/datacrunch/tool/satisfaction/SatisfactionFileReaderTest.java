package org.gs4tr.dataforce.datacrunch.tool.satisfaction;

import java.util.List;

import org.gs4tr.dataforce.datacrunch.tool.service.text.TextFileReaderService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.impl.TextFileReaderServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class SatisfactionFileReaderTest
{
    @TestConfiguration
    static class SatisfactionFileReaderContextConfiguration
    {
        @Bean
        public TextFileReaderService textFileReaderService()
        {
            return new TextFileReaderServiceImpl();
        }
    }

    @Autowired
    private TextFileReaderService textFileReaderService;

    @Test
    public void satisfactionFileReaderTest1() throws Exception
    {
        String satisfactionFile = "target/test-classes/" +
            "KomodoSatisfactionFeedback_Subject001_Session001_US-Speaker-2019-06-Subject-1.txt";
        List<String> contents = textFileReaderService.getContents(satisfactionFile);
        Assert.assertEquals(47, contents.size());
        Assert.assertEquals("20190610 10:16:06,1,😖", contents.get(0));
    }
}
