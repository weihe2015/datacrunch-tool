package org.gs4tr.dataforce.datacrunch.tool.hypothesis;

import org.gs4tr.dataforce.datacrunch.tool.service.text.TextFileReaderService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.impl.TextFileReaderServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class HypothesisFileReaderTest
{
    @TestConfiguration
    static class PromptFileReaderContextConfiguration
    {
        @Bean
        public TextFileReaderService xmlReaderService()
        {
            return new TextFileReaderServiceImpl();
        }
    }

    @Autowired
    private TextFileReaderService textFileReaderService;

    @Test
    public void splitGoogle001CSVTest() throws Exception
    {
        String str = "\"Jun 21, 2019, 1:12:47 PM EDT\",Play the song again,1730";
        String[] splitted = str.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
        Assert.assertEquals(3, splitted.length);
        Assert.assertEquals("\"Jun 21, 2019, 1:12:47 PM EDT\"", splitted[0]);
        Assert.assertEquals("Play the song again", splitted[1]);
        Assert.assertEquals("1730", splitted[2]);
    }
}
