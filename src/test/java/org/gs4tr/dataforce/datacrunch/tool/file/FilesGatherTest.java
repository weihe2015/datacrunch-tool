package org.gs4tr.dataforce.datacrunch.tool.file;

import java.util.ArrayList;
import java.util.List;

import org.gs4tr.dataforce.datacrunch.tool.service.file.FileService;
import org.gs4tr.dataforce.datacrunch.tool.service.file.impl.FileServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class FilesGatherTest
{
    @TestConfiguration
    static class FileServiceContextConfiguration
    {
        @Bean
        public FileService fileService()
        {
            return new FileServiceImpl();
        }
    }

    @Autowired
    private FileService fileService;

    @Test
    public void getAllFiles() throws Exception
    {
        String folder = "target/test-classes/output";
        List<String> anvilFiles = new ArrayList<>();
        List<String> hypothesisFiles = new ArrayList<>();
        List<String> promptFiles = new ArrayList<>();
        List<String> satisfactionFiles = new ArrayList<>();

        fileService.getFiles(folder, anvilFiles, hypothesisFiles, promptFiles, satisfactionFiles);

        Assert.assertEquals(12, anvilFiles.size());
        Assert.assertEquals(4, hypothesisFiles.size());
        Assert.assertEquals(10, promptFiles.size());
        Assert.assertEquals(6, satisfactionFiles.size());

        Assert.assertTrue(anvilFiles.get(0).endsWith("s007_Amazon_session2_pr001_20190710.anvil"));
        Assert.assertTrue(anvilFiles.get(1).endsWith("s007_Amazon_session2_pr002_20190710.anvil"));
        Assert.assertTrue(anvilFiles.get(2).endsWith("s007_Amazon_session2_pr003_20190710.anvil"));

        Assert.assertTrue(hypothesisFiles.get(0).endsWith("Amazon_US_Speaker_2019-06-10_to_2019-06-24.csv"));
        Assert.assertTrue(hypothesisFiles.get(1).endsWith("Apple_US_Speaker_2019-06-03_to_2019-06-18.csv"));
        Assert.assertTrue(hypothesisFiles.get(2).endsWith("Google001.csv"));
        Assert.assertTrue(hypothesisFiles.get(3).endsWith("MyActivity.html"));

        Assert.assertTrue(promptFiles.get(0).endsWith("US-Speaker-2019-06-Subject-001.csv"));
        Assert.assertTrue(promptFiles.get(1).endsWith("US-Speaker-2019-06-Subject-002.csv"));
        Assert.assertTrue(promptFiles.get(2).endsWith("US-Speaker-2019-06-Subject-003.csv"));

        Assert.assertTrue(satisfactionFiles.get(0).endsWith(
            "KomodoSatisfactionFeedback_Subject001_Session001_US-Speaker-2019-06-Subject-1.txt"));
        Assert.assertTrue(satisfactionFiles.get(1).endsWith(
            "KomodoSatisfactionFeedback_Subject001_Session002_US-Speaker-2019-06-Subject-1.txt"));
        Assert.assertTrue(satisfactionFiles.get(2).endsWith(
            "KomodoSatisfactionFeedback_Subject001_Session003_US-Speaker-2019-06-Subject-1.txt"));
    }
}
