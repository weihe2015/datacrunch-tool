package org.gs4tr.dataforce.datacrunch.tool.prompt;

import java.io.File;
import java.util.List;

import org.gs4tr.dataforce.datacrunch.tool.service.text.TextFileReaderService;
import org.gs4tr.dataforce.datacrunch.tool.service.text.impl.TextFileReaderServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class PromptFileReaderTest
{
    @TestConfiguration
    static class PromptFileReaderContextConfiguration
    {
        @Bean
        public TextFileReaderService xmlReaderService()
        {
            return new TextFileReaderServiceImpl();
        }
    }

    @Autowired
    private TextFileReaderService textFileReaderService;

    @Test
    public void promptFileReaderTest1() throws Exception
    {
        String testFilePath = "target/test-classes/US-Speaker-2019-06-Subject-015.csv";
        File testFile = new File(testFilePath);
        Assert.assertTrue(testFile.exists());

        List<String> contents = textFileReaderService.getContents(testFilePath);
        Assert.assertEquals(47, contents.size());

        Assert.assertEquals("\"How tall is (monument, statue or building)\",encyclopedia,Book,", contents.get(0));
        Assert.assertEquals("Ask [device] a question about the weather,weather,Book,", contents.get(1));
    }

}
