package org.gs4tr.dataforce.datacrunch.tool.satisfaction;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class SatisfactionFileNameTest
{

    @Test
    public void convertDateTest() throws Exception
    {
        String str = "20190710 16:53:43";
        Date date = new SimpleDateFormat("yyyyMMdd HH:mm:ss").parse(str);
        Assert.assertTrue(date.before(new Date()));
    }
}
