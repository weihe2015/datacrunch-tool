package org.gs4tr.dataforce.datacrunch.tool.anvil;

import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.gs4tr.dataforce.datacrunch.tool.service.file.FilenameService;
import org.gs4tr.dataforce.datacrunch.tool.service.file.impl.FilenameServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class AnvilFileNameTest
{
    @TestConfiguration
    static class FilenameServiceContextConfiguration
    {
        @Bean
        public FilenameService filenameService()
        {
            return new FilenameServiceImpl();
        }
    }

    @Autowired
    private FilenameService filenameService;

    @Test
    public void anvilFileNameInfoTest1()
    {
        String anvilFilename = "s007_Amazon_session2_pr001_20190710.anvil";
        Map<String, String> infoMap = filenameService.getInfoMapFromAnvilFileName(anvilFilename);
        Assert.assertFalse(infoMap.isEmpty());
        Assert.assertEquals("007", infoMap.get(FilenameService.SUBJECT_ID));
        Assert.assertEquals("Amazon", infoMap.get(FilenameService.COMPANY));
        Assert.assertEquals("2", infoMap.get(FilenameService.SESSION_ID));
        Assert.assertEquals("001", infoMap.get(FilenameService.PROMPT_ID));
    }

    @Test
    public void anvilFileNameInfoTest2()
    {
        String anvilFilename = "s007_Apple_session1_pr001_20190710.anvil";
        Map<String, String> infoMap = filenameService.getInfoMapFromAnvilFileName(anvilFilename);
        Assert.assertFalse(infoMap.isEmpty());
        Assert.assertEquals("007", infoMap.get(FilenameService.SUBJECT_ID));
        Assert.assertEquals("Apple", infoMap.get(FilenameService.COMPANY));
        Assert.assertEquals("1", infoMap.get(FilenameService.SESSION_ID));
        Assert.assertEquals("001", infoMap.get(FilenameService.PROMPT_ID));
    }

    @Test
    public void anvilFileNameInfoTest3()
    {
        String anvilFilename = "s034_Google_session3_pr001_20190630.anvil";
        Map<String, String> infoMap = filenameService.getInfoMapFromAnvilFileName(anvilFilename);
        Assert.assertFalse(infoMap.isEmpty());
        Assert.assertEquals("034", infoMap.get(FilenameService.SUBJECT_ID));
        Assert.assertEquals("Google", infoMap.get(FilenameService.COMPANY));
        Assert.assertEquals("3", infoMap.get(FilenameService.SESSION_ID));
        Assert.assertEquals("001", infoMap.get(FilenameService.PROMPT_ID));
    }

    @Test
    public void videoFileNameTest()
    {
        String videoFilename = "../Subject007/s007_Amazon_session2_pr001_20190710.avi";
        Assert.assertEquals("s007_Amazon_session2_pr001_20190710.avi", FilenameUtils.getName(videoFilename));
    }
}
