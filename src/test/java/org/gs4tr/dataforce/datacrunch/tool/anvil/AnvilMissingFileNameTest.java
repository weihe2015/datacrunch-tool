package org.gs4tr.dataforce.datacrunch.tool.anvil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.gs4tr.dataforce.datacrunch.tool.service.file.FileService;
import org.gs4tr.dataforce.datacrunch.tool.service.file.FilenameService;
import org.gs4tr.dataforce.datacrunch.tool.service.file.impl.FileServiceImpl;
import org.gs4tr.dataforce.datacrunch.tool.service.file.impl.FilenameServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class AnvilMissingFileNameTest
{
    @TestConfiguration
    static class AnvilMissingFilenameContextConfiguration
    {
        @Bean
        public FilenameService filenameService()
        {
            return new FilenameServiceImpl();
        }

        @Bean
        public FileService fileService()
        {
            return new FileServiceImpl();
        }
    }

    @Autowired
    private FileService fileService;

    @Autowired
    private FilenameService filenameService;

    @Test
    public void anvilReferenceFileNameTest1() throws Exception
    {
        String folder = "target/test-classes/anvilFileNames";

        List<String> anvilFiles = new ArrayList<>();
        List<String> hypothesisFiles = new ArrayList<>();
        List<String> promptFiles = new ArrayList<>();
        List<String> satisfactionFiles = new ArrayList<>();

        fileService.getFiles(folder, anvilFiles, hypothesisFiles, promptFiles, satisfactionFiles);

        for (String anvilFilePath : anvilFiles)
        {
            Map<String, String> infoMap = filenameService.getInfoMapFromAnvilFileName(anvilFilePath);
            Assert.assertTrue(infoMap.containsKey(FilenameService.SUBJECT_ID));
            Assert.assertTrue(infoMap.containsKey(FilenameService.SESSION_ID));
            Assert.assertTrue(infoMap.containsKey(FilenameService.PROMPT_ID));
        }
    }
}
