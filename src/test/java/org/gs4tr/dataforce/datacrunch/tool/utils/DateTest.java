package org.gs4tr.dataforce.datacrunch.tool.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

public class DateTest
{
    @Test
    public void dateFormatTest() throws Exception
    {
        String str = "Jun 21, 2019, 1:17:53 PM EDT";
        Date date = null;
        try
        {
            date = new SimpleDateFormat("MMM d, yyyy, HH:mm:ss a zzz").parse(str);
        }
        catch (ParseException ex)
        {
            ex.printStackTrace();
        }
        Assert.assertNotNull(date);
    }

    @Test
    public void dateFormatTest2() throws Exception
    {
        String str = "20190710 16:53:43";
        Date date = new SimpleDateFormat("yyyyMMdd HH:mm:ss").parse(str);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        System.err.println(new SimpleDateFormat("yyyyMMdd").format(date));
    }

    @Test
    public void numberTest1() throws Exception
    {
        int num = 1;
        String str = String.format("%03d", num);
        System.out.println(str);
    }
}
