package org.gs4tr.dataforce.datacrunch.tool.html;

import java.io.File;
import java.util.regex.Matcher;

import org.gs4tr.dataforce.datacrunch.tool.service.html.HtmlReaderService;
import org.gs4tr.dataforce.datacrunch.tool.service.html.impl.HtmlReaderServiceImpl;
import org.gs4tr.dataforce.datacrunch.tool.service.text.HypothesisFileService;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

@RunWith(SpringRunner.class)
public class HtmlFileReaderTest
{
    @TestConfiguration
    static class HtmlFileReaderTestContextConfiguration
    {
        @Bean
        public HtmlReaderService htmlReaderService()
        {
            return new HtmlReaderServiceImpl();
        }
    }

    @Autowired
    private HtmlReaderService htmlReaderService;

    @Test
    public void htmlReaderTest1() throws Exception
    {
        String testFilePath = "target/test-classes/MyActivity.html";
        File testFile = new File(testFilePath);
        Assert.assertTrue(testFile.exists());

        Document doc = htmlReaderService.getDocument(testFilePath);
        Elements elements = htmlReaderService.getContentDivElements(doc);
        Assert.assertTrue(elements.size() > 0);

        for (Element element : elements)
        {
            String text = element.wholeText();
            if (!StringUtils.startsWithIgnoreCase(text, "Said"))
            {
                continue;
            }
            String wholeText = element.wholeText();
            Matcher matcher = HypothesisFileService.CONTENT_PATTERN.matcher(wholeText);
            Assert.assertTrue(matcher.find());
            Assert.assertEquals(4, matcher.groupCount());
        }
    }
}
