package org.gs4tr.dataforce.datacrunch.tool.prompt;

import java.util.Map;

import org.gs4tr.dataforce.datacrunch.tool.service.file.FilenameService;
import org.gs4tr.dataforce.datacrunch.tool.service.file.impl.FilenameServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class PromptFileNameTest
{
    @TestConfiguration
    static class FilenameServiceContextConfiguration
    {
        @Bean
        public FilenameService filenameService()
        {
            return new FilenameServiceImpl();
        }
    }

    @Autowired
    private FilenameService filenameService;

    @Test
    public void promptFilenameInfoTest1()
    {
        String promptFileName = "US-Speaker-2019-06-Subject-005.csv";
        Map<String, String> infoMap = filenameService.getInfoMapFromPromptFileName(promptFileName);
        Assert.assertFalse(infoMap.isEmpty());
        Assert.assertEquals("005", infoMap.get(FilenameServiceImpl.SUBJECT_ID));
    }
}
